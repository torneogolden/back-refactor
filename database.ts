/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { createConnection } from 'typeorm';
export const connectDB = async () => {
  await createConnection();
};
