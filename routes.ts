import { SchemaValidation } from './src/middlewares/ValidationSchema';
import {
  CanchaController,
  EquipoController,
  FixtureController,
  NoticiaController,
  PosicionController,
  ReglamentoController,
  SancionController,
  TorneoController,
  ZonaController
} from './src/controller';

/**
 * Rutas del microservicio.
 */
export const AppRoutes = [
  // region soporte
  {
    path: '/canchas/obtener',
    method: 'get',
    action: CanchaController.getAll,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/canchas/registrar',
    method: 'post',
    action: CanchaController.register,
    schema: SchemaValidation.schemaEmpty
  },
  // endregion
  // region torneo
  {
    path: '/torneo/vigentes',
    method: 'get',
    action: TorneoController.getCurrent,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/torneo/equipos/todos/:idTournament',
    method: 'get',
    action: TorneoController.getAllTeamsByTournament,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/torneo/obtener/:name',
    method: 'get',
    action: TorneoController.getByName,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/torneo/goleadores/:idTournament',
    method: 'get',
    action: TorneoController.getScorersByTournament,
    schema: SchemaValidation.schemaEmpty
  },
  // endregion
  // region noticias
  {
    path: '/noticia/principales/:idTournament',
    method: 'get',
    action: NoticiaController.mainNews,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/noticia/secundarias/:idTournament',
    method: 'get',
    action: NoticiaController.secondaryNews,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/noticia/obtener/:idNews',
    method: 'get',
    action: NoticiaController.getById,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/noticia/historicas/:idTournament',
    method: 'get',
    action: NoticiaController.historicalNews,
    schema: SchemaValidation.schemaEmpty
  },
  // endregion
  // region reglamento
  {
    path: '/reglamento/obtener/:idTournament',
    method: 'get',
    action: ReglamentoController.getRegulationById,
    schema: SchemaValidation.schemaEmpty
  },
  // endregion
  // region fixture
  {
    path: '/fixture/fecha/obtener/:idTournament',
    method: 'post',
    action: FixtureController.getFixtureById,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/fixture/fecha/partidos/:idTournament',
    method: 'post',
    action: FixtureController.getMatchesById,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/fixture/fecha/disputadas/:idTournament',
    method: 'post',
    action: FixtureController.getSchedulesPlayedById,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/fixture/fecha/disputados/:idTournament',
    method: 'post',
    action: FixtureController.getMatchesPlayedById,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/fixture/playoff/:idTournament',
    method: 'post',
    action: FixtureController.getPlayoffByTournament,
    schema: SchemaValidation.schemaEmpty
  },
  // endregion
  // region posiciones
  {
    path: '/posicion/general/:idTournament',
    method: 'get',
    action: PosicionController.getPositions,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/posicion/zonas/:idTournament',
    method: 'get',
    action: PosicionController.getPositionsByZone,
    schema: SchemaValidation.schemaEmpty
  },
  // endregion
  // region sanciones
  {
    path: '/sancion/equipos/:idPashe',
    method: 'post',
    action: SancionController.getTeamSanctions,
    schema: SchemaValidation.schemaEmpty
  },
  // endregion
  // region zonas
  {
    path: '/zona/todos/:idTournament',
    method: 'post',
    action: ZonaController.getZonesByTournament,
    schema: SchemaValidation.schemaEmpty
  },
  // endregion
  // region equipos
  {
    path: '/equipo/obtener/:idTeam',
    method: 'post',
    action: EquipoController.getTeamById,
    schema: SchemaValidation.schemaEmpty
  },
  {
    path: '/equipo/jugadores/:idTeam/:idTournament',
    method: 'post',
    action: EquipoController.getPlayersByTeam,
    schema: SchemaValidation.schemaEmpty
  }
  // endregion
];
