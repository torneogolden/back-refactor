FROM node:12-buster-slim
ARG environment=desarrollo
ENV MYSQL mysql
RUN apt-get update
RUN npm i -g pm2@4.5.6
WORKDIR /api-torneo
COPY package.json ./package.json
ENV UV_THREADPOOL_SIZE=32
RUN npm install
COPY . ./
COPY ormconfig-$environment.json ./ormconfig.json
RUN npm run build
RUN mkdir /api-torneo/dist/UploadedFiles
RUN pm2 dump
EXPOSE 3000
CMD [ "pm2-runtime", "npm", "--", "start" ]