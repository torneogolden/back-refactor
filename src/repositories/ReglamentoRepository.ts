import { plainToClass } from 'class-transformer';
import { injectable } from 'inversify';
import { getRepository, Repository } from 'typeorm';
import { Reglamentos } from '../data';
import { Reglamento } from '../models';

export interface IReglamentoRepository {
  getById(idTournament?: number): Promise<Reglamento>;
}

@injectable()
export class ReglamentoRepository implements IReglamentoRepository {
  async getById(idTournament?: number): Promise<Reglamento> {
    const reglamentoRepository: Repository<Reglamentos> = getRepository(Reglamentos);
    const entities = await reglamentoRepository.findOne({ where: { idTorneo: idTournament } });
    const result: Reglamento = plainToClass(Reglamento, entities);
    return result;
  }
}
