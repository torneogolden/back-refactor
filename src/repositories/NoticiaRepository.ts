import { plainToClass } from 'class-transformer';
import { id, injectable } from 'inversify';
import { getRepository, IsNull, Not, Repository } from 'typeorm';
import { Noticias } from '../data';
import { Noticia } from '../models';

export interface INoticiaRepository {
  mainNews(idTournament?: number): Promise<Noticia>;
  secondaryNews(idTournament?: number): Promise<Noticia[]>;
  getById(idNews?: number): Promise<Noticia>;
  historicalNews(idTournament?: number): Promise<Noticia[]>;
}

@injectable()
export class NoticiaRepository implements INoticiaRepository {
  async mainNews(idTournament?: number): Promise<Noticia> {
    const newsRepository: Repository<Noticias> = getRepository(Noticias);
    const entities = await newsRepository.findOne({
      relations: ['torneo', 'club', 'categoriaNoticia', 'thumbnail'],
      where: [
        { idCategoriaNoticia: 1, idTorneo: idTournament },
        { idCategoriaNoticia: 1, idTorneo: IsNull() }
      ],
      order: {
        idNoticia: 'DESC'
      }
    });
    const result = plainToClass(Noticia, entities);
    return result;
  }
  async secondaryNews(idTournament?: number): Promise<Noticia[]> {
    const newsRepository: Repository<Noticias> = getRepository(Noticias);
    const entities = await newsRepository.find({
      relations: ['torneo', 'club', 'categoriaNoticia', 'thumbnail'],
      where: [
        { idCategoriaNoticia: 2, idTorneo: idTournament },
        { idCategoriaNoticia: 2, idTorneo: IsNull() }
      ],
      order: {
        idNoticia: 'DESC'
      },
      take: 3
    });
    const result = plainToClass(Noticia, entities);
    return result;
  }
  async getById(idNews?: number): Promise<Noticia> {
    const newsRepository: Repository<Noticias> = getRepository(Noticias);
    const entities = await newsRepository.findOne({ relations: ['torneo', 'club', 'categoriaNoticia', 'thumbnail'], where: { idNoticia: idNews } });
    const result = plainToClass(Noticia, entities);
    return result;
  }
  async historicalNews(idTournament?: number): Promise<Noticia[]> {
    const newsRepository: Repository<Noticias> = getRepository(Noticias);
    const entities = await newsRepository.find({
      relations: ['torneo', 'club', 'categoriaNoticia', 'thumbnail'],
      where: [{ idTorneo: idTournament }, { idTorneo: IsNull() }],
      order: {
        idNoticia: 'DESC'
      },
      take: 10
    });
    const result = plainToClass(Noticia, entities);
    return result;
  }
}
