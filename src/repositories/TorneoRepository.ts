import { plainToClass } from 'class-transformer';
import { id, injectable } from 'inversify';
import { getRepository, In, IsNull, Not, Repository, WhereExpression } from 'typeorm';
import { Equipos, Files, Goleadores, Torneos } from '../data';
import { Equipo, Goleador, Torneo } from '../models';

export interface ITorneoRepository {
  getCurrent(): Promise<Torneo[]>;
  getAllTeamsByTournament(idTournament: number): Promise<Equipo[]>;
  getByName(name?: string): Promise<Torneo>;
  getScorersByTournament(idTournament?: number): Promise<Goleador[]>;
}

@injectable()
export class TorneoRepository implements ITorneoRepository {
  async getCurrent(): Promise<Torneo[]> {
    const tournamentRepository: Repository<Torneos> = getRepository(Torneos);
    const entities = await tournamentRepository.find({ relations: ['fase'], where: { idEstado: IsNull() } });
    const result: Torneo[] = plainToClass(Torneo, entities);
    return result;
  }

  async getAllTeamsByTournament(idTournament?: number): Promise<Equipo[]> {
    let result: Equipo[] = [];
    if (idTournament) {
      const teamsRepository: Repository<Equipos> = getRepository(Equipos);
      const entities = await teamsRepository.find({ relations: ['files'], where: { idTorneo: idTournament }, order: { nombre: 'ASC' } });
      result = plainToClass(Equipo, entities);
    }
    return result;
  }

  async getScorersByTournament(idTournament?: number): Promise<Goleador[]> {
    const scorersRepository: Repository<Goleadores> = getRepository(Goleadores);
    const entities = await scorersRepository.find({
      relations: ['equipo', 'equipo.files', 'jugador', 'jugador.persona'],
      where: [{ idTorneo: idTournament }],
      order: {
        cantidadGoles: 'DESC'
      },
      take: 20
    });
    const result = plainToClass(Goleador, entities);
    return result;
  }

  /* Migracion tabla archivos
      const ent = await teamsRepository.find();
    const filesRepository: Repository<Files> = getRepository(Files);
    for (const iterator of ent) {
      console.log(iterator.logo + iterator.camiseta + iterator.camisetalogo);
      const schedules = [iterator.logo, iterator.camiseta, iterator.camisetalogo];
      if (schedules && schedules.length) {
        const files = await filesRepository.find({ where: [{ id: In(schedules) }] });
        for (const file of files) {
          file.idEquipo = iterator.idEquipo;
          file.idTipoArchivo = file.idSeccion == 'ESCUDOS' ? 1 : file.idSeccion == 'CAMISETAS' ? 2 : file.idSeccion == 'CAMISETAESCUDO' ? 3 : file.idSeccion == 'NOTICIAS' ? 4 : file.idSeccion == 'JUGADORES' ? 5 : 0;
          filesRepository.save(file);
        }
      }
    }
  */

  async getByName(name?: string): Promise<Torneo> {
    const tournamentRepository: Repository<Torneos> = getRepository(Torneos);
    const entities = await tournamentRepository.findOne({ relations: ['fase'], where: { nombre: name, idEstado: IsNull() } });
    const result = plainToClass(Torneo, entities);
    return result;
  }
}
