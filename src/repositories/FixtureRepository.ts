import { plainToClass } from 'class-transformer';
import { injectable } from 'inversify';
import moment from 'moment';
import { Brackets, getRepository, In, IsNull, Repository } from 'typeorm';
import { Fechas, FixtureZona, Partidos, Playoff } from '../data';
import { Fecha, Partido, PlayOff } from '../models';

export interface IFixtureRepository {
  getById(idTournament: number, schedule: Fecha): Promise<Fecha[]>;
  getMatchesById(idTournament: number, schedule: Fecha): Promise<Partido[]>;
  getSchedulesPlayedById(idTournament: number): Promise<Fecha[]>;
  getMatchesPlayedById(idTournament: number, schedule: Fecha): Promise<Partido[]>;
  getPlayoffByTournament(idTournament: number): Promise<PlayOff[]>;
}

@injectable()
export class FixtureRepository implements IFixtureRepository {
  async getById(idTournament: number, schedule: Fecha): Promise<Fecha[]> {
    const fixtureRepository: Repository<FixtureZona> = getRepository(FixtureZona);
    const entities = await fixtureRepository
      .createQueryBuilder('fixture_zona')
      .select('f.fecha', 'fecha')
      .innerJoin(Fechas, 'f', 'f.id_fixture_zona = fixture_zona.id_fixture')
      .innerJoin(Partidos, 'p', 'f.id_fecha = p.id_fecha')
      .where('fixture_zona.id_torneo = :id', { id: idTournament })
      .groupBy('f.fecha')
      .orderBy('f.fecha')
      .getRawMany();
    const result: Fecha[] = plainToClass(Fecha, entities);
    return result;
  }

  async getSchedulesPlayedById(idTournament: number): Promise<Fecha[]> {
    const fixtureRepository: Repository<FixtureZona> = getRepository(FixtureZona);
    const entities = await fixtureRepository
      .createQueryBuilder('fixture_zona')
      .select('f.fecha', 'fecha')
      .innerJoin(Fechas, 'f', 'f.id_fixture_zona = fixture_zona.id_fixture')
      .innerJoin(Partidos, 'p', 'f.id_fecha = p.id_fecha')
      .where('fixture_zona.id_torneo = :id', { id: idTournament })
      .andWhere(new Brackets((qb) => qb.where('p.idResultado is not null').orWhere('p.idResultadosZona is not null')))
      .groupBy('f.fecha')
      .orderBy('f.fecha')
      .getRawMany();
    const result: Fecha[] = plainToClass(Fecha, entities);
    return result;
  }

  async getMatchesPlayedById(idTournament: number, schedule: Fecha): Promise<Partido[]> {
    const formattedDate = moment(schedule.fecha).format('yyyy-MM-DD');
    const fechasRepository: Repository<Fechas> = getRepository(Fechas);
    const partidoRepository: Repository<Partidos> = getRepository(Partidos);
    const schedules = await fechasRepository
      .createQueryBuilder('fechas')
      .innerJoinAndSelect('fechas.fixtureZona', 'fixture')
      .where('fechas.fecha = :formattedDate', { formattedDate: formattedDate })
      .andWhere('fixture.id_torneo = :idTorneo', { idTorneo: idTournament })
      .getMany();
    const matches = await partidoRepository
      .createQueryBuilder('partidos')
      .where({ idFecha: In(schedules.map((x) => x.idFecha)) })
      .innerJoinAndSelect('partidos.localB', 'localB')
      .innerJoinAndSelect('partidos.visitanteB', 'visitanteB')
      .innerJoinAndSelect('localB.files', 'localB.files')
      .innerJoinAndSelect('visitanteB.files', 'visitanteB.files')
      .leftJoinAndSelect('partidos.goles', 'goles')
      .leftJoinAndSelect('partidos.sanciones', 'sanciones')
      .innerJoinAndSelect('goles.jugador', 'jugador')
      .innerJoinAndSelect('sanciones.jugador', 'jugadorB')
      .innerJoinAndSelect('sanciones.tipo', 'tipo')
      .innerJoinAndSelect('jugador.persona', 'p')
      .innerJoinAndSelect('jugadorB.persona', 's')
      .cache(true)
      .getMany();

    const result: Partido[] = plainToClass(Partido, matches);
    return result;
  }

  async getPlayoffByTournament(idTournament: number): Promise<PlayOff[]> {
    const playOffRepository: Repository<Playoff> = getRepository(Playoff);
    const matches = await playOffRepository
      .createQueryBuilder('playoff')
      .where({ idTorneo: idTournament })
      .innerJoinAndSelect('playoff.partido', 'partido')
      .innerJoinAndSelect('playoff.torneo', 'torneo')
      .innerJoinAndSelect('playoff.llave', 'llave')
      .innerJoinAndSelect('playoff.etapa', 'etapa')
      .innerJoinAndSelect('playoff.local', 'local')
      .innerJoinAndSelect('local.files', 'files')
      .innerJoinAndSelect('playoff.visitante', 'visitante')
      .innerJoinAndSelect('visitante.files', 'filesV')
      .leftJoinAndSelect('playoff.ganador', 'ganador')
      .leftJoinAndSelect('ganador.files', 'filesG')
      .leftJoinAndSelect('partido.goles', 'goles')
      .cache(true)
      .getMany();

    const result: PlayOff[] = plainToClass(PlayOff, matches);
    return result;
  }

  async getMatchesById(idTournament: number, schedule: Fecha): Promise<Partido[]> {
    const fixtureRepository: Repository<FixtureZona> = getRepository(FixtureZona);
    const formattedDate = moment(schedule.fecha).format('yyyy-MM-DD');
    const entities = await fixtureRepository
      .createQueryBuilder('fixture_zona')
      .select('f.id_fecha', 'idFecha')
      .innerJoin(Fechas, 'f', 'f.id_fixture_zona = fixture_zona.id_fixture')
      .where('fixture_zona.id_torneo = :id', { id: idTournament })
      .andWhere('f.fecha = :fecha', { fecha: formattedDate })
      .getRawMany();
    const partidoRepository: Repository<Partidos> = getRepository(Partidos);
    const schedules = entities.map((date) => date.idFecha);
    const matches = await partidoRepository.find({
      relations: ['localB', 'visitanteB', 'horario', 'cancha', 'visitanteB.files', 'localB.files'],
      where: [{ idFecha: In(schedules) }]
    });
    const result: Partido[] = plainToClass(Partido, matches);
    return result;
  }
}
