import { plainToClass } from 'class-transformer';
import { injectable } from 'inversify';
import { getRepository, Repository } from 'typeorm';
import { Canchas, Domicilios } from '../data';
import { Cancha, CustomResponse } from '../models';

export interface ICanchaRepository {
  getAll(): Promise<Cancha[]>;
  register(cancha: Cancha): Promise<CustomResponse>;
}

@injectable()
export class CanchaRepository implements ICanchaRepository {
  async getAll(): Promise<Cancha[]> {
    const canchaRepository: Repository<Canchas> = getRepository(Canchas);
    const entities = await canchaRepository.find({ relations: ['domicilio', 'club'] });
    const result: Cancha[] = plainToClass(Cancha, entities);
    return result;
  }

  async register(cancha: Cancha): Promise<CustomResponse> {
    const canchaRepository: Repository<Canchas> = getRepository(Canchas);
    const entities = await canchaRepository.findOne({ nombre: cancha.nombre });
    const res = new CustomResponse();
    if (!entities) {
      let domicilio: any;
      const domRepository: Repository<Domicilios> = getRepository(Domicilios);
      const dom = await domRepository.findOne({ calle: cancha.domicilio.calle, numeracion: cancha.domicilio.numeracion, barrio: cancha.domicilio.barrio });
      if (!dom) {
        domicilio = await domRepository.save({
          barrio: cancha.domicilio.barrio,
          observaciones: cancha.domicilio.observaciones,
          calle: cancha.domicilio.calle,
          numeracion: cancha.domicilio.numeracion
        });
      }
      await canchaRepository.insert({
        idClub: cancha.club.idClub,
        nombre: cancha.nombre,
        capacidad: cancha.capacidad,
        idDomicilio: domicilio?.idDomicilio ? domicilio.idDomicilio : dom.idDomicilio
      });
    } else {
      res.mensaje = 'La cancha que intenta registrar ya existe.';
      res.error = true;
    }
    return res;
  }
}
