import { plainToClass } from 'class-transformer';
import { injectable } from 'inversify';
import { getRepository, Repository } from 'typeorm';
import { Equipos, Jugadores, Personas } from '../data';
import { Equipo, Jugador } from '../models';

export interface IEquipoRepository {
  getTeamById(idTeam?: number): Promise<Equipo>;
  getPlayersByTeam(idTeam: number, idTournament?: number): Promise<Jugador[]>;
}

@injectable()
export class EquipoRepository implements IEquipoRepository {
  async getTeamById(idTeam?: number): Promise<Equipo> {
    const teamsRepository: Repository<Equipos> = getRepository(Equipos);
    const entities = await teamsRepository.findOne({ relations: ['files', 'club', 'categoriaEquipo', 'torneo'], where: { idEquipo: idTeam } });
    const result = plainToClass(Equipo, entities);
    return result;
  }

  async getPlayersByTeam(idTeam: number, idTournament?: number): Promise<Jugador[]> {
    const personRepository: Repository<Personas> = getRepository(Personas);
    const entities = await personRepository
      .createQueryBuilder('personas')
      .innerJoinAndSelect('personas.foto', 'foto')
      .innerJoinAndMapOne('personas.jugadores', Jugadores, 'jugadores', 'jugadores.id_persona=personas.id_persona')
      .innerJoinAndSelect('jugadores.equipo', 'equipo')
      .leftJoinAndSelect('jugadores.goles', 'goles')
      .leftJoinAndSelect('jugadores.sanciones', 'sanciones')
      .leftJoinAndSelect('sanciones.fechaFinB', 'fechaFin')
      .leftJoinAndSelect('sanciones.tipo', 'tipo')
      .where('equipo.id_torneo = :idTournament', { idTournament: idTournament })
      .andWhere('jugadores.id_equipo = :idTeam', { idTeam: idTeam })
      .orderBy('personas.apellido', 'ASC')
      .getMany();
    const result = plainToClass(Jugador, entities);
    return result;
  }
}
