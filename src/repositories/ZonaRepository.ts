import { plainToClass } from 'class-transformer';
import { injectable } from 'inversify';
import { getRepository, Repository } from 'typeorm';
import { EquiposZona, Zonas } from '../data';
import { Zona } from '../models';

export interface IZonaRepository {
  getZonesByTournament(idTournament?: number): Promise<Zona[]>;
}

@injectable()
export class ZonaRepository implements IZonaRepository {
  async getZonesByTournament(idTournament?: number): Promise<Zona[]> {
    const zonesRepository: Repository<Zonas> = getRepository(Zonas);
    const entities = await zonesRepository
      .createQueryBuilder('zonas')
      .innerJoinAndSelect('zonas.torneo', 'torneo')
      .innerJoinAndSelect('zonas.equiposZonas', 'equiposZonas')
      .innerJoinAndSelect('equiposZonas.equipo', 'equipo')
      .innerJoinAndSelect('equipo.files', 'files')
      .where({ idTorneo: idTournament })
      .andWhere('torneo.id_fase = zonas.id_fase')
      .getMany();

    entities.forEach(
      (x) =>
        (x.equiposZonas = plainToClass(
          EquiposZona,
          x.equiposZonas.map((y) => y.equipo)
        ))
    );
    const result: Zona[] = plainToClass(Zona, entities);
    return result;
  }
}
