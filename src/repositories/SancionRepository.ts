import { plainToClass } from 'class-transformer';
import { injectable } from 'inversify';
import { Brackets, getRepository, In, Repository } from 'typeorm';
import { SancionesEquipo } from '../data';
import { SancionEquipo, SancionEquipoComando } from '../models';

export interface ISancionRepository {
  getTeamSanctions(teamSanctionsCommand: SancionEquipoComando[], idPashe: number): Promise<SancionEquipo[]>;
}

@injectable()
export class SancionRepository implements ISancionRepository {
  async getTeamSanctions(teamSanctionsCommand: SancionEquipoComando[], idPashe: number): Promise<SancionEquipo[]> {
    const teamSanctionsRepository: Repository<SancionesEquipo> = getRepository(SancionesEquipo);
    let entities = await teamSanctionsRepository
      .createQueryBuilder('sancionesEquipo')
      .innerJoinAndSelect('sancionesEquipo.equipo', 'equipo')
      .leftJoinAndSelect('sancionesEquipo.zona', 'zona')
      .where({ idEquipo: In(teamSanctionsCommand.map((x) => x.idEquipo)) })
      .getMany();

    entities = entities.filter((x) =>
      teamSanctionsCommand.find((y) => y.idZona === x.idZona || !y.idZona) && teamSanctionsCommand.find((y) => y.idTorneo === x.idTorneo) ? x : undefined
    );
    const result: SancionEquipo[] = plainToClass(SancionEquipo, entities);
    return result;
  }
}
