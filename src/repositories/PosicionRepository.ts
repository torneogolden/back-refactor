import { plainToClass } from 'class-transformer';
import { injectable } from 'inversify';
import { getRepository, Repository } from 'typeorm';
import { Posiciones, Zonas } from '../data';
import { Equipo, Posicion, PosicionZona } from '../models';

export interface IPosicionRepository {
  getPositions(idTournament?: number): Promise<Posicion[]>;
  getPositionsByZone(idTournament?: number): Promise<Array<PosicionZona[]>>;
}

@injectable()
export class PosicionRepository implements IPosicionRepository {
  async getPositions(idTournament?: number): Promise<Posicion[]> {
    const positionsRepository: Repository<Posiciones> = getRepository(Posiciones);
    const positions = await positionsRepository
      .createQueryBuilder('posiciones')
      .where({ idTorneo: idTournament })
      .innerJoinAndSelect('posiciones.equipo', 'equipo')
      .innerJoinAndSelect('equipo.files', 'f')
      .cache(true)
      .orderBy('posiciones.puntos', 'DESC')
      .addOrderBy('posiciones.difGol', 'DESC')
      .addOrderBy('posiciones.golesFavor', 'DESC')
      .addOrderBy('posiciones.fairPlay', 'DESC')
      .getMany();

    const result = plainToClass(Posicion, positions);
    return result;
  }
  async getPositionsByZone(idTournament?: number): Promise<Array<PosicionZona[]>> {
    const zonesRepository: Repository<Zonas> = getRepository(Zonas);
    const zonesAndPositions = new Array<PosicionZona[]>();
    const positions = await zonesRepository
      .createQueryBuilder('zonas')
      .where({ idTorneo: idTournament })
      .andWhere('zonas.idFase = 2')
      .innerJoinAndSelect('zonas.posicionesZonas', 'posicionesZonas')
      .innerJoinAndSelect('posicionesZonas.equipo', 'equipoZona')
      .innerJoinAndSelect('equipoZona.equipo', 'equipo')
      .innerJoinAndSelect('equipo.files', 'f')
      .innerJoinAndSelect('posicionesZonas.zona', 'zona')
      .orderBy('zona.descripcion', 'ASC')
      .addOrderBy('posicionesZonas.puntos', 'DESC')
      .addOrderBy('posicionesZonas.difGol', 'DESC')
      .addOrderBy('posicionesZonas.golesFavor', 'DESC')
      .addOrderBy('posicionesZonas.fairPlay', 'DESC')
      .cache(true)
      .getMany();

    positions.forEach((x) => {
      const positionsZones: PosicionZona[] = [];
      x.posicionesZonas.forEach((y) => {
        const pos = plainToClass(PosicionZona, y);
        pos.equipo = plainToClass(Equipo, y.equipo.equipo);
        positionsZones.push(pos);
      });
      zonesAndPositions.push(positionsZones);
    });
    return zonesAndPositions;
  }
}
