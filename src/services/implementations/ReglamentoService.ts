/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { logger } from '../../logger/CustomLogger';
import { Reglamento } from '../../models';
import { IReglamentoRepository } from '../../repositories';
import { IReglamentoService } from '../interfaces';
import { TYPES } from '../types/types';

@injectable()
export class ReglamentoService implements IReglamentoService {
  private readonly _reglamentoRepository: IReglamentoRepository;

  constructor(
    @inject(TYPES.ReglamentoRepository)
    repository: IReglamentoRepository
  ) {
    this._reglamentoRepository = repository;
  }

  async getById(idTournament?: number): Promise<Reglamento> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._reglamentoRepository.getById(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
