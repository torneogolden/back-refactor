/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { logger } from '../../logger/CustomLogger';
import { Cancha, CustomResponse } from '../../models';
import { ICanchaRepository } from '../../repositories/CanchaRepository';
import { ICanchaService } from '../interfaces/ICanchaService';
import { TYPES } from '../types/types';

/**
 * Servicio que tiene como responsabilidad
 * todo lo vinculado a el tratamiento de activacion de beneficiarios
 */
@injectable()
export class CanchaService implements ICanchaService {
  private readonly _canchaRepository: ICanchaRepository;

  constructor(
    @inject(TYPES.CanchaRepository)
    repository: ICanchaRepository
  ) {
    this._canchaRepository = repository;
  }

  /**
   * Método asíncrono para obtener los datos del beneficiario.
   * @param {payload} - es el comando del cual se buscan sus datos personales del beneficiario.
   * @returns {Beneficiario} - devuelve los datos personales del beneficiarios
   */
  async getAll(): Promise<any[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._canchaRepository.getAll();
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }

  /**
   * Método asíncrono para obtener los datos del beneficiario.
   * @param {payload} - es el comando del cual se buscan sus datos personales del beneficiario.
   * @returns {Beneficiario} - devuelve los datos personales del beneficiarios
   */
  async register(cancha: Cancha): Promise<CustomResponse> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._canchaRepository.register(cancha);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
