/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { logger } from '../../logger/CustomLogger';
import { Fecha, Partido, PlayOff } from '../../models';
import { IFixtureRepository } from '../../repositories/FixtureRepository';
import { IFixtureService } from '../interfaces';
import { TYPES } from '../types/types';

@injectable()
export class FixtureService implements IFixtureService {
  private readonly _fixtureRepository: IFixtureRepository;

  constructor(
    @inject(TYPES.FixtureRepository)
    repository: IFixtureRepository
  ) {
    this._fixtureRepository = repository;
  }

  async getById(idTournament: number, schedule: Fecha): Promise<Fecha[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._fixtureRepository.getById(idTournament, schedule);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
  async getSchedulesPlayedById(idTournament: number): Promise<Fecha[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._fixtureRepository.getSchedulesPlayedById(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
  async getMatchesPlayedById(idTournament: number, schedule: Fecha): Promise<Partido[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._fixtureRepository.getMatchesPlayedById(idTournament, schedule);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
  async getPlayoffByTournament(idTournament: number): Promise<PlayOff[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._fixtureRepository.getPlayoffByTournament(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
  async getMatchesById(idTournament: number, schedule: Fecha): Promise<Partido[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._fixtureRepository.getMatchesById(idTournament, schedule);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
