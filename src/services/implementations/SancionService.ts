/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { logger } from '../../logger/CustomLogger';
import { SancionEquipo, SancionEquipoComando } from '../../models';
import { ISancionRepository } from '../../repositories';
import { ISancionService } from '../interfaces';
import { TYPES } from '../types/types';

@injectable()
export class SancionService implements ISancionService {
  private readonly _sanctionRepository: ISancionRepository;

  constructor(
    @inject(TYPES.SancionRepository)
    repository: ISancionRepository
  ) {
    this._sanctionRepository = repository;
  }

  async getTeamSanctions(teamSanctionsCommand: SancionEquipoComando[], idPashe: number): Promise<SancionEquipo[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._sanctionRepository.getTeamSanctions(teamSanctionsCommand, idPashe);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
