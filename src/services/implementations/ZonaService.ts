/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { logger } from '../../logger/CustomLogger';
import { Zona } from '../../models';
import { IZonaRepository } from '../../repositories';
import { IZonaService } from '../interfaces';
import { TYPES } from '../types/types';

@injectable()
export class ZonaService implements IZonaService {
  private readonly _zoneRepository: IZonaRepository;

  constructor(
    @inject(TYPES.ZonaRepository)
    repository: IZonaRepository
  ) {
    this._zoneRepository = repository;
  }

  async getZonesByTournament(idTournament?: number): Promise<Zona[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._zoneRepository.getZonesByTournament(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
