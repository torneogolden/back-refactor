/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { TYPES } from '../types/types';
import { logger } from '../../logger/CustomLogger';
import { ITorneoService } from '../interfaces';
import { ITorneoRepository } from '../../repositories';
import { Equipo, Goleador, Torneo } from '../../models';

@injectable()
export class TorneoService implements ITorneoService {
  private readonly _torneoRepository: ITorneoRepository;

  constructor(
    @inject(TYPES.TorneoRepository)
    repository: ITorneoRepository
  ) {
    this._torneoRepository = repository;
  }

  /**
   * Método asíncrono para obtener los datos del beneficiario.
   * @param {payload} - es el comando del cual se buscan sus datos personales del beneficiario.
   * @returns {Beneficiario} - devuelve los datos personales del beneficiarios
   */
  async getCurrent(): Promise<Torneo[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._torneoRepository.getCurrent();
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }

  async getAllTeamsByTournament(idTournament: number): Promise<Equipo[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._torneoRepository.getAllTeamsByTournament(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }

  async getScorersByTournament(idTournament: number): Promise<Goleador[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._torneoRepository.getScorersByTournament(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }

  async getByName(name: string): Promise<Torneo> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._torneoRepository.getByName(name);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
