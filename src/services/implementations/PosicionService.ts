/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { TYPES } from '../types/types';
import { logger } from '../../logger/CustomLogger';
import { Posicion, PosicionZona } from '../../models';
import { IPosicionRepository } from '../../repositories';
import { IPosicionService } from '../interfaces';

@injectable()
export class PosicionService implements IPosicionService {
  private readonly _positionsRepository: IPosicionRepository;

  constructor(
    @inject(TYPES.PosicionRepository)
    repository: IPosicionRepository
  ) {
    this._positionsRepository = repository;
  }

  async getPositions(idTournament: number): Promise<Posicion[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._positionsRepository.getPositions(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }

  async getPositionsByZone(idTournament: number): Promise<Array<PosicionZona[]>> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._positionsRepository.getPositionsByZone(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
