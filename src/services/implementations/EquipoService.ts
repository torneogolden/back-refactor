/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { TYPES } from '../types/types';
import { logger } from '../../logger/CustomLogger';
import { Equipo, Jugador } from '../../models';
import { IEquipoService } from '../interfaces';
import { IEquipoRepository } from '../../repositories';

@injectable()
export class EquipoService implements IEquipoService {
  private readonly _teamRepository: IEquipoRepository;

  constructor(
    @inject(TYPES.EquipoRepository)
    repository: IEquipoRepository
  ) {
    this._teamRepository = repository;
  }

  async getTeamById(idTeam: number): Promise<Equipo> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._teamRepository.getTeamById(idTeam);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
  async getPlayersByTeam(idTeam: number, idTournament: number): Promise<Jugador[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._teamRepository.getPlayersByTeam(idTeam, idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
