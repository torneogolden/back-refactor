/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { TYPES } from '../types/types';
import { logger } from '../../logger/CustomLogger';
import { INoticiaService } from '../interfaces';
import { INoticiaRepository } from '../../repositories';
import { Noticia } from '../../models';

@injectable()
export class NoticiaService implements INoticiaService {
  private readonly _noticiaRepository: INoticiaRepository;

  constructor(
    @inject(TYPES.NoticiaRepository)
    repository: INoticiaRepository
  ) {
    this._noticiaRepository = repository;
  }
  async mainNews(idTournament: number): Promise<Noticia> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._noticiaRepository.mainNews(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
  async secondaryNews(idTournament: number): Promise<Noticia[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._noticiaRepository.secondaryNews(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
  async getById(idNews: number): Promise<Noticia> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._noticiaRepository.getById(idNews);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
  async historicalNews(idTournament: number): Promise<Noticia[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this._noticiaRepository.historicalNews(idTournament);
        resolve(result);
      } catch (e) {
        logger.error(e);
        reject(e);
      }
    });
  }
}
