import { Container } from 'inversify';
import { TYPES } from './types/types';
import {
  ICanchaService,
  IEquipoService,
  IFixtureService,
  INoticiaService,
  IPosicionService,
  IReglamentoService,
  ISancionService,
  ITorneoService,
  IZonaService
} from './interfaces';
import { CanchaService, EquipoService, FixtureService, NoticiaService, PosicionService, ReglamentoService, SancionService, TorneoService, ZonaService } from './implementations';
import {
  CanchaRepository,
  EquipoRepository,
  FixtureRepository,
  ICanchaRepository,
  IEquipoRepository,
  IFixtureRepository,
  INoticiaRepository,
  IPosicionRepository,
  IReglamentoRepository,
  ISancionRepository,
  ITorneoRepository,
  IZonaRepository,
  NoticiaRepository,
  PosicionRepository,
  ReglamentoRepository,
  SancionRepository,
  TorneoRepository,
  ZonaRepository
} from '../repositories';

/**SS
 * Clase encargada de hacer el registro de todas las interfaces, con sus respectivos tipos e implementaciones
 * para que queden disponibles en el contenedor de injección de dependencias.
 */

const container = new Container();
container.bind<ICanchaService>(TYPES.CanchaService).to(CanchaService);
container.bind<ICanchaRepository>(TYPES.CanchaRepository).to(CanchaRepository);
container.bind<ITorneoService>(TYPES.TorneoService).to(TorneoService);
container.bind<ITorneoRepository>(TYPES.TorneoRepository).to(TorneoRepository);
container.bind<INoticiaService>(TYPES.NoticiaService).to(NoticiaService);
container.bind<INoticiaRepository>(TYPES.NoticiaRepository).to(NoticiaRepository);
container.bind<IReglamentoService>(TYPES.ReglamentoService).to(ReglamentoService);
container.bind<IReglamentoRepository>(TYPES.ReglamentoRepository).to(ReglamentoRepository);
container.bind<IFixtureService>(TYPES.FixtureService).to(FixtureService);
container.bind<IFixtureRepository>(TYPES.FixtureRepository).to(FixtureRepository);
container.bind<IPosicionService>(TYPES.PosicionService).to(PosicionService);
container.bind<IPosicionRepository>(TYPES.PosicionRepository).to(PosicionRepository);
container.bind<ISancionService>(TYPES.SancionService).to(SancionService);
container.bind<ISancionRepository>(TYPES.SancionRepository).to(SancionRepository);
container.bind<IZonaService>(TYPES.ZonaService).to(ZonaService);
container.bind<IZonaRepository>(TYPES.ZonaRepository).to(ZonaRepository);
container.bind<IEquipoService>(TYPES.EquipoService).to(EquipoService);
container.bind<IEquipoRepository>(TYPES.EquipoRepository).to(EquipoRepository);
export default container;
