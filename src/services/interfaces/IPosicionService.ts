export interface IPosicionService {
  getPositions(idTournament: number): Promise<unknown[]>;
  getPositionsByZone(idTournament: number): Promise<unknown[]>;
}
