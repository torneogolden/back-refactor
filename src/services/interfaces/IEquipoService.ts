export interface IEquipoService {
  getTeamById(idTeam: number): Promise<unknown>;
  getPlayersByTeam(idTeam: number, idTournament: number): Promise<unknown[]>;
}
