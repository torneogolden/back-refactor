import { Fecha, Partido } from '../../models';

export interface IFixtureService {
  getById(idTournament: number, schedule: Fecha): Promise<unknown[]>;
  getMatchesById(idTournament: number, schedule: Fecha): Promise<unknown[]>;
  getSchedulesPlayedById(idTournament: number): Promise<unknown[]>;
  getMatchesPlayedById(idTournament: number, schedule: Fecha): Promise<unknown[]>;
  getPlayoffByTournament(idTournament: number): Promise<unknown[]>;
}
