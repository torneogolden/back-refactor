export interface ITorneoService {
  getCurrent(): Promise<any[]>;
  getAllTeamsByTournament(idTournament: number): Promise<any[]>;
  getByName(name: string): Promise<any>;
  getScorersByTournament(idTournament: number): Promise<any[]>;
}
