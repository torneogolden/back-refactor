export interface IZonaService {
  getZonesByTournament(idTournament?: number): Promise<unknown>;
}
