import { Cancha, CustomResponse } from '../../models';

export interface ICanchaService {
  getAll(): Promise<any[]>;
  register(cancha: Cancha): Promise<CustomResponse>;
}
