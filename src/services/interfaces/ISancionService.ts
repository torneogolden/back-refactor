import { SancionEquipoComando } from '../../models';

export interface ISancionService {
  getTeamSanctions(teamSanctionsCommand: SancionEquipoComando[], idPashe: number): Promise<unknown>;
}
