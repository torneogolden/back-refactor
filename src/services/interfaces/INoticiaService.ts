export interface INoticiaService {
  historicalNews(idTournament: number): Promise<unknown[]>;
  getById(idNews: number): Promise<unknown>;
  secondaryNews(idTournament: number): Promise<unknown[]>;
  mainNews(idTournament: number): Promise<unknown>;
}
