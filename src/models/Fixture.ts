import { Torneo, Fecha, TipoFixture } from './index';

export class Fixture {
  public idFixture: number;
  public torneo: Torneo;
  public fechas: Fecha[];
  public tipoFixture: TipoFixture;

  constructor(idFixture: number, torneo: Torneo, fechas: Fecha[], tipoFixture: TipoFixture) {
    this.idFixture = idFixture;
    this.torneo = torneo;
    this.fechas = fechas;
    this.tipoFixture = tipoFixture;
  }
}
