export class Categoria {
  public descripcion: string;
  public idCategoria: number;

  constructor(descripcion: string, idCategoria: number) {
    this.descripcion = descripcion;
    this.idCategoria = idCategoria;
  }
}
