import { Jugador } from './index';
export class IEquipo {
  nombre: string;
  idEquipo: number;
  imagePath: string;
  logo: number;
  jugadores: Jugador[];

  constructor(nombre: string, idEquipo: number, imagePath: string, logo: number, jugadores: Jugador[]) {
    this.nombre = nombre;
    this.idEquipo = idEquipo;
    this.imagePath = imagePath;
    this.logo = logo;
    this.jugadores = jugadores;
  }
}
