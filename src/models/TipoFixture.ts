export class TipoFixture {
  public idTipo: number;
  public descripcion: string;

  constructor(idTipo: number, descripcion: string) {
    this.idTipo = idTipo;
    this.descripcion = descripcion;
  }
}
