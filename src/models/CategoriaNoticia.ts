export class CategoriaNoticia {
  public idCategoriaNoticia: number;
  public descripcion: string;

  constructor(idCategoriaNoticia: number, descripcion: string) {
    this.idCategoriaNoticia = idCategoriaNoticia;
    this.descripcion = descripcion;
  }
}
