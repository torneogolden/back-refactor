import { Expose, Type } from 'class-transformer';
import { Categoria, Club, Torneo, File } from './index';

export class Equipo {
  public idEquipo: number;
  public nombre: string;
  public descripcion: string;
  public fechaAlta: Date;
  public logo: number;
  public camiseta: number;
  public camisetaLogo: number;
  @Expose({ name: 'categoriaEquipo' })
  @Type(() => Categoria)
  public categoria: Categoria;
  public club: Club;
  public torneo: Torneo;
  public fairPlay: number;
  @Expose({ name: 'files' })
  @Type(() => File)
  files: File[];

  constructor(
    idEquipo: number,
    nombre: string,
    descripcion: string,
    fechaAlta: Date,
    logo: number,
    camiseta: number,
    camisetaLogo: number,
    categoria: Categoria,
    club: Club,
    torneo: Torneo,
    fairPlay: number,
    files: File[]
  ) {
    this.idEquipo = idEquipo;
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.fechaAlta = fechaAlta;
    this.logo = logo;
    this.camiseta = camiseta;
    this.camisetaLogo = camisetaLogo;
    this.categoria = categoria;
    this.club = club;
    this.torneo = torneo;
    this.fairPlay = fairPlay;
    this.files = files;
  }
}
