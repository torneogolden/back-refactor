export class Permiso {
  public descripcion: string;
  public fechaExpiracion: Date;

  constructor(descripcion: string, fechaExpiracion: Date) {
    this.descripcion = descripcion;
    this.fechaExpiracion = fechaExpiracion;
  }
}
