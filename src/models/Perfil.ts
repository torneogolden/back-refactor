import { Permiso } from './index';

export class Perfil {
  public idPerfil: number;
  public nPerfil: string;
  public permisos: Permiso[];

  constructor(idPerfil: number, nPerfil: string, permisos: Permiso[]) {
    this.idPerfil = idPerfil;
    this.nPerfil = nPerfil;
    this.permisos = permisos;
  }
}
