import { Expose } from 'class-transformer';

export class TipoSancion {
  public descripcion: string;
  @Expose({ name: 'idTipo' })
  public id: number;

  constructor(descripcion: string, id: number) {
    this.descripcion = descripcion;
    this.id = id;
  }
}
