export class Reglamento {
  public idReglamento: number;
  public descripcion: string;
  public idTorneo: number;

  constructor(idReglamento: number, descripcion: string, idTorneo: number) {
    this.idReglamento = idReglamento;
    this.descripcion = descripcion;
    this.idTorneo = idTorneo;
  }
}
