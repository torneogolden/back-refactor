export class TipoTorneo {
  public descripcion: string;
  public idTipo: number;

  constructor(descripcion: string, idTipo: number) {
    this.descripcion = descripcion;
    this.idTipo = idTipo;
  }
}
