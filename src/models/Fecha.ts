import { EstadoFecha, Partido, Fixture, IPartido } from './index';

export class Fecha {
  public idFecha: number;
  public fecha: Date;
  public estado: EstadoFecha;
  public partidos: Partido[];
  public iPartidos: IPartido[];
  public fixture: Fixture;

  constructor(idFecha: number, fecha: Date, estado: EstadoFecha, partidos: Partido[], iPartidos: IPartido[], fixture: Fixture) {
    this.idFecha = idFecha;
    this.fecha = fecha;
    this.estado = estado;
    this.partidos = partidos;
    this.iPartidos = iPartidos;
    this.fixture = fixture;
  }
}
