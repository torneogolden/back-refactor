export class CustomResponse {
  error: boolean;
  mensaje: string;
  id: number;

  constructor(error = false, mensaje = 'La operación se realizo con éxito.', id?: number) {
    this.error = error;
    this.mensaje = mensaje;
    this.id = id;
  }
}
