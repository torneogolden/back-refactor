import { Persona } from './index';

export class Veedor extends Persona {
  public idVeedor: number;
  public fechaAlta: Date;

  constructor(idVeedor: number, fechaAlta: Date) {
    super();
    this.idVeedor = idVeedor;
    this.fechaAlta = fechaAlta;
  }
}
