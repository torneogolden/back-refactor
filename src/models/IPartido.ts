import { IEquipo, Cancha, HorarioFijo, Gol, Sancion, Jugador, Llave, Fecha, Resultado, ResultadoZona, Etapa } from './index';

export class IPartido {
  idPartido: number;
  local: IEquipo[];
  visitante: IEquipo[];
  cancha: Cancha;
  horario: HorarioFijo;
  fecha: Fecha;
  idFixture: number;
  golesLocal: Gol[];
  golesVisitante: Gol[];
  sancionesLocal: Sancion[];
  sancionesVisitante: Sancion[];
  resultado: Resultado;
  resultadoZona: ResultadoZona;
  jugadorLocal: Jugador;
  jugadorVisitante: Jugador;
  llave: Llave;
  desImagenes: boolean;
  desImagenesV: boolean;
  etapa: Etapa;
  equiposPartido: IEquipo[];
  ganadorPlayoff: IEquipo;
  penales: boolean;
  detallePenales: string;
  golesABorrar: Gol[];

  constructor(
    idPartido: number,
    local: IEquipo[],
    visitante: IEquipo[],
    cancha: Cancha,
    horario: HorarioFijo,
    fecha: Fecha,
    idFixture: number,
    golesLocal: Gol[],
    golesVisitante: Gol[],
    sancionesLocal: Sancion[],
    sancionesVisitante: Sancion[],
    resultado: Resultado,
    resultadoZona: ResultadoZona,
    jugadorLocal: Jugador,
    jugadorVisitante: Jugador,
    llave: Llave,
    desImagenes: boolean,
    desImagenesV: boolean,
    etapa: Etapa,
    equiposPartido: IEquipo[],
    ganadorPlayoff: IEquipo,
    penales: boolean,
    detallePenales: string,
    golesABorrar: Gol[]
  ) {
    this.idPartido = idPartido;
    this.local = local;
    this.visitante = visitante;
    this.cancha = cancha;
    this.horario = horario;
    this.fecha = fecha;
    this.idFixture = idFixture;
    this.golesLocal = golesLocal;
    this.golesVisitante = golesVisitante;
    this.sancionesLocal = sancionesLocal;
    this.sancionesVisitante = sancionesVisitante;
    this.resultado = resultado;
    this.resultadoZona = resultadoZona;
    this.jugadorLocal = jugadorLocal;
    this.jugadorVisitante = jugadorVisitante;
    this.llave = llave;
    this.desImagenes = desImagenes;
    this.desImagenesV = desImagenesV;
    this.etapa = etapa;
    this.equiposPartido = equiposPartido;
    this.ganadorPlayoff = ganadorPlayoff;
    this.penales = penales;
    this.detallePenales = detallePenales;
    this.golesABorrar = golesABorrar;
  }
}
