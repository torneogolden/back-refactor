import { TipoTorneo, Categoria, Regla, Modalidad, Equipo, Fase, EstadoTorneo } from './index';

export class Torneo {
  public idTorneo: number;
  public nombre: string;
  public descripcion: string;
  public fechaInicio: Date;
  public fechafin: Date;
  public tipoTorneo: TipoTorneo;
  public categoria: Categoria;
  public regla: Regla;
  public modalidad: Modalidad;
  public equipos: Equipo[];
  public fase: Fase;
  public estado: EstadoTorneo;

  constructor(
    idTorneo: number,
    nombre: string,
    descripcion: string,
    fechaInicio: Date,
    fechafin: Date,
    tipoTorneo: TipoTorneo,
    categoria: Categoria,
    regla: Regla,
    modalidad: Modalidad,
    equipos: Equipo[],
    fase: Fase,
    estado: EstadoTorneo
  ) {
    this.idTorneo = idTorneo;
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.fechaInicio = fechaInicio;
    this.fechafin = fechafin;
    this.tipoTorneo = tipoTorneo;
    this.categoria = categoria;
    this.regla = regla;
    this.modalidad = modalidad;
    this.equipos = equipos;
    this.fase = fase;
    this.estado = estado;
  }
}
