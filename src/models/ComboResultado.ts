import { Expose } from 'class-transformer';

export class ComboResultado {
  @Expose({ name: 'Id' })
  id: number;
  @Expose({ name: 'Nombre' })
  nombre: string;
  @Expose({ name: 'Abreviatura' })
  abreviatura: string;

  constructor(id?: number, nombre?: string, abreviatura?: string) {
    this.id = id;
    this.nombre = nombre;
    this.abreviatura = abreviatura;
  }
}
