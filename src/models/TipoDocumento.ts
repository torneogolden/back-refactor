export class TipoDocumento {
  public idTipoDocumento: number;
  public descripcion: string;

  constructor(idTipoDocumento: number, descripcion: string) {
    this.idTipoDocumento = idTipoDocumento;
    this.descripcion = descripcion;
  }
}
