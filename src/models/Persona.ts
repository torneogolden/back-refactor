import { TipoDocumento, Contacto, Domicilio, File } from './index';

export class Persona {
  public nombre: string;
  public apellido: string;
  public fechaNacimiento: Date;
  public nroDocumento: number;
  public tipoDocumento: TipoDocumento;
  public domicilio: Domicilio;
  public contacto: Contacto;
  public idPersona: number;
  public edad: number;
  public ocupacion: string;
  public idFoto: number;
  foto: File;

  constructor(
    nombre?: string,
    apellido?: string,
    fechaNacimiento?: Date,
    nroDocumento?: number,
    tipoDocumento?: TipoDocumento,
    domicilio?: Domicilio,
    contacto?: Contacto,
    idPersona?: number,
    edad?: number,
    ocupacion?: string,
    idFoto?: number,
    foto?: File
  ) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.fechaNacimiento = fechaNacimiento;
    this.nroDocumento = nroDocumento;
    this.tipoDocumento = tipoDocumento;
    this.domicilio = domicilio;
    this.contacto = contacto;
    this.idPersona = idPersona;
    this.edad = edad;
    this.ocupacion = ocupacion;
    this.idFoto = idFoto;
    this.foto = foto;
  }
}
