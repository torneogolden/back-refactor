export * from './Persona';
export * from './Arbitro';
export * from './Cancha';
export * from './CategoriaEquipo';
export * from './CategoriaNoticia';
export * from './CategoriaTorneo';
export * from './Club';
export * from './Contacto';
export * from './Domicilio';
export * from './Equipo';
export * from './EstadosTorneo';
export * from './EstadoFecha';
export * from './EstadoPartido';
export * from './EtapaPlayoff';
export * from './Fase';
export * from './Fecha';
export * from './Fixture';
export * from './Gol';
export * from './Goleador';
export * from './HorarioFijo';
export * from './IEquipo';
export * from './IPartido';
export * from './Jugador';
export * from './Llave';
export * from './Localidad';
export * from './ModalidadTorneo';
export * from './Noticia';
export * from './ParametrosFixture';
export * from './Partido';
export * from './Perfil';
export * from './Permiso';
export * from './PlayOff';
export * from './Posicion';
export * from './PosicionZona';
export * from './Provincia';
export * from './Regla';
export * from './Reglamento';
export * from './ReglaTorneo';
export * from './Resultado';
export * from './ResultadoZona';
export * from './Sancion';
export * from './SancionEquipo';
export * from './TipoDocumento';
export * from './TipoFixture';
export * from './TipoSancion';
export * from './TipoTorneo';
export * from './Torneo';
export * from './Turno';
export * from './Usuario';
export * from './Veedor';
export * from './Zona';
export * from './CustomResponse';
export * from './HttpCodes';
export * from './SancionEquipoComando';
export * from './File';
