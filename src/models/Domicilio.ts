import { Provincia } from './Provincia';

export class Domicilio {
  idDomicilio: number;
  calle: string;
  numeracion: number;
  piso?: number;
  dpto: string;
  torre?: number;
  barrio: string;
  observaciones: string;
  provincia: Provincia;

  constructor(idDomicilio: number, calle: string, numeracion: number, piso: number, dpto: string, torre: number, barrio: string, observaciones: string, provincia: Provincia) {
    this.idDomicilio = idDomicilio;
    this.calle = calle;
    this.numeracion = numeracion;
    this.piso = piso;
    this.dpto = dpto;
    this.torre = torre;
    this.barrio = barrio;
    this.observaciones = observaciones;
    this.provincia = provincia;
  }
}
