export class Club {
  idClub: number;
  nombre: string;
  descripcion: string;

  constructor(idClub: number, nombre: string, descripcion: string) {
    this.idClub = idClub;
    this.nombre = nombre;
    this.descripcion = descripcion;
  }
}
