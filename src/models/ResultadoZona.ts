import { Equipo, Zona } from './index';

export class ResultadoZona {
  public idResultado: number;
  public ganador: Equipo;
  public perdedor: Equipo;
  public empate: number;
  public zona: Zona;

  constructor(idResultado: number, ganador: Equipo, perdedor: Equipo, empate: number, zona: Zona) {
    this.idResultado = idResultado;
    this.ganador = ganador;
    this.perdedor = perdedor;
    this.empate = empate;
    this.zona = zona;
  }
}
