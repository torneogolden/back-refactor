import { Expose, Type } from 'class-transformer';
import { Localidad } from './index';

export class Provincia {
  @Expose({ name: 'id_provincia' })
  idProvincia: number;
  nombre: string;
  @Expose({ name: 'fecha_alta' })
  fechaAlta: Date;
  @Expose({ name: 'fecha_modificacion' })
  fechaModificacion: Date;
  @Expose({ name: 'fecha_baja' })
  fechaBaja: Date;
  @Type(() => Localidad)
  localidades: Localidad[];
  constructor(idProvincia: number, nombre: string, fechaAlta: Date, fechaModificacion: Date, fechaBaja: Date, localidades: Localidad[]) {
    this.idProvincia = idProvincia;
    this.nombre = nombre;
    this.fechaAlta = fechaAlta;
    this.fechaModificacion = fechaModificacion;
    this.fechaBaja = fechaBaja;
    this.localidades = localidades;
  }
}
