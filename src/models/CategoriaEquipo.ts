export class CategoriaEquipo {
  public descripcion: string;
  public id: number;

  constructor(descripcion: string, id: number) {
    this.descripcion = descripcion;
    this.id = id;
  }
}
