export class SancionEquipoComando {
  idEquipo: number;
  idTorneo: number;
  idZona: number;

  constructor(idEquipo?: number, idTorneo?: number, idZona?: number) {
    this.idEquipo = idEquipo;
    this.idTorneo = idTorneo;
    this.idZona = idZona;
  }
}
