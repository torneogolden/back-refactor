import { TipoFixture, Zona } from './index';

export class ParametrosFixture {
  public zona: Zona;
  public idTorneo: number;
  public idFase: number;
  public cantidadDiasEntrePartidos: number;
  public tipoDeFixture: TipoFixture;
  public esInterzonal: boolean;
  public intercalarLocalVisitante: boolean;
  public fechaInicioFixture: Date;

  constructor(
    zona: Zona,
    idTorneo: number,
    idFase: number,
    cantidadDiasEntrePartidos: number,
    tipoDeFixture: TipoFixture,
    esInterzonal: boolean,
    intercalarLocalVisitante: boolean,
    fechaInicioFixture: Date
  ) {
    this.zona = zona;
    this.idTorneo = idTorneo;
    this.idFase = idFase;
    this.cantidadDiasEntrePartidos = cantidadDiasEntrePartidos;
    this.tipoDeFixture = tipoDeFixture;
    this.esInterzonal = esInterzonal;
    this.intercalarLocalVisitante = intercalarLocalVisitante;
    this.fechaInicioFixture = fechaInicioFixture;
  }
}
