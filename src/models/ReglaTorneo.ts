import { Torneo } from './index';
export class ReglaTorneo {
  public descripcion: string;
  public idRegla: number;
  public torneo: Torneo;

  constructor(descripcion: string, idRegla: number, torneo: Torneo) {
    this.descripcion = descripcion;
    this.idRegla = idRegla;
    this.torneo = torneo;
  }
}
