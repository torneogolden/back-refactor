import { Equipo } from './index';

export class Resultado {
  public idResultado: number;
  public ganador: Equipo;
  public perdedor: Equipo;
  public empate: number;

  constructor(idResultado: number, ganador: Equipo, perdedor: Equipo, empate: number) {
    this.idResultado = idResultado;
    this.ganador = ganador;
    this.perdedor = perdedor;
    this.empate = empate;
  }
}
