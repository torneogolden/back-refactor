export class Contacto {
  public idContacto: number;
  public telefonoMovil: string;
  public teleFonofijo: string;
  public email: string;

  constructor(idContacto: number, telefonoMovil: string, teleFonofijo: string, email: string) {
    this.idContacto = idContacto;
    this.telefonoMovil = telefonoMovil;
    this.teleFonofijo = teleFonofijo;
    this.email = email;
  }
}
