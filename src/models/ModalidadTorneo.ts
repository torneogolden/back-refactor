export class Modalidad {
  public descripcion: string;
  public idModalidad: number;

  constructor(descripcion: string, idModalidad: number) {
    this.descripcion = descripcion;
    this.idModalidad = idModalidad;
  }
}
