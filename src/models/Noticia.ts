import { Expose, Type } from 'class-transformer';
import { File } from './File';
import { Torneo, Club, CategoriaNoticia } from './index';

export class Noticia {
  public idNoticia: number;
  public titulo: string;
  public descripcion: string;
  public fecha: Date;
  public torneo: Torneo;
  public club: Club;
  public categoriaNoticia: CategoriaNoticia;
  public tags: string;
  public idThumbnail: number;
  @Expose({ name: 'thumbnail' })
  @Type(() => File)
  file: File;

  constructor(
    idNoticia: number,
    titulo: string,
    descripcion: string,
    fecha: Date,
    torneo: Torneo,
    club: Club,
    categoriaNoticia: CategoriaNoticia,
    tags: string,
    idThumbnail: number,
    file?: File
  ) {
    this.file = file;
    this.idNoticia = idNoticia;
    this.titulo = titulo;
    this.descripcion = descripcion;
    this.fecha = fecha;
    this.torneo = torneo;
    this.club = club;
    this.categoriaNoticia = categoriaNoticia;
    this.tags = tags;
    this.idThumbnail = idThumbnail;
  }
}
