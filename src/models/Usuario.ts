import { Perfil } from './index';

export class Usuario {
  public idUsuario: number;
  public nUsuario: string;
  public password: string;
  public perfil: Perfil;
  public caducidad: Date;

  constructor(idUsuario: number, nUsuario: string, password: string, perfil: Perfil, caducidad: Date) {
    this.idUsuario = idUsuario;
    this.nUsuario = nUsuario;
    this.password = password;
    this.perfil = perfil;
    this.caducidad = caducidad;
  }
}
