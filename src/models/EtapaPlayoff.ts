export class Etapa {
  public idEtapa: number;
  public descripcion: string;

  constructor(idEtapa: number, descripcion: string) {
    this.idEtapa = idEtapa;
    this.descripcion = descripcion;
  }
}
