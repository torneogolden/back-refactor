import { Expose, Type } from 'class-transformer';
import { Domicilio, Club } from './index';

export class Cancha {
  idCancha: number;
  nombre: string;
  capacidad: number;
  @Expose({ name: 'domicilio' })
  @Type(() => Domicilio)
  domicilio: Domicilio;
  @Expose({ name: 'club' })
  @Type(() => Club)
  club: Club;
  constructor(idCancha: number, nombre: string, capacidad: number, domicilio: Domicilio, club: Club) {
    this.idCancha = idCancha;
    this.nombre = nombre;
    this.capacidad = capacidad;
    this.domicilio = domicilio;
    this.club = club;
  }
}
