export class Fase {
  public idFase: number;
  public descripcion: string;

  constructor(idFase: number, descripcion: string) {
    this.idFase = idFase;
    this.descripcion = descripcion;
  }
}
