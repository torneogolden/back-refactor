import { Expose, Type } from 'class-transformer';
import { Equipo, Partido, Jugador } from './index';

export class Gol {
  public idGol: number;
  public minuto: Date;
  @Expose({ name: 'jugador' })
  @Type(() => Jugador)
  public jugador: Jugador;
  public partido: Partido;
  public equipo: Equipo;

  constructor(idGol: number, minuto: Date, jugador: Jugador, partido: Partido, equipo: Equipo) {
    this.idGol = idGol;
    this.minuto = minuto;
    this.jugador = jugador;
    this.partido = partido;
    this.equipo = equipo;
  }
}
