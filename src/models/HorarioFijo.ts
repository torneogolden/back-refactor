import { Turno } from './index';

export class HorarioFijo {
  public idHorario: number;
  public turno: Turno;
  public inicio: string;
  public fin: string;

  constructor(idHorario: number, turno: Turno, inicio: string, fin: string) {
    this.idHorario = idHorario;
    this.turno = turno;
    this.inicio = inicio;
    this.fin = fin;
  }
}
