export class Regla {
  public descripcion: string;
  public idRegla: number;

  constructor(descripcion: string, idRegla: number) {
    this.descripcion = descripcion;
    this.idRegla = idRegla;
  }
}
