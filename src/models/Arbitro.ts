import { Persona } from './index';

export class Arbitro extends Persona {
  public idArbitro: number;
  public matricula: number;

  constructor(idArbitro: number, matricula: number) {
    super();
    this.idArbitro = idArbitro;
    this.matricula = matricula;
  }
}
