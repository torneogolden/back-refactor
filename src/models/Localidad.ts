import { Expose } from 'class-transformer';

export class Localidad {
  @Expose({ name: 'id_localidad' })
  idLocalidad: number;
  nombre: string;
  @Expose({ name: 'fecha_alta' })
  fechaAlta: Date;
  @Expose({ name: 'fecha_modificacion' })
  fechaModificacion: Date;
  @Expose({ name: 'fecha_baja' })
  fechaBaja: Date;
  constructor(idLocalidad: number, nombre: string, fechaAlta: Date, fechaModificacion: Date, fechaBaja: Date) {
    this.idLocalidad = idLocalidad;
    this.nombre = nombre;
    this.fechaAlta = fechaAlta;
    this.fechaModificacion = fechaModificacion;
    this.fechaBaja = fechaBaja;
  }
}
