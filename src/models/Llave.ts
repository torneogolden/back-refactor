export class Llave {
  public idLlave: number;
  public descripcion: string;

  constructor(idLlave: number, descripcion: string) {
    this.idLlave = idLlave;
    this.descripcion = descripcion;
  }
}
