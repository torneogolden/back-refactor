export class EstadoFecha {
  public descripcion: string;
  public idEstado: number;

  constructor(descripcion: string, idEstado: number) {
    this.descripcion = descripcion;
    this.idEstado = idEstado;
  }
}
