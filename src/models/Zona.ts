import { Expose, Transform, Type } from 'class-transformer';
import { Equipo, Torneo, Fase } from './index';

export class Zona {
  public idZona: number;
  public descripcion: string;
  public torneo: Torneo;
  @Expose({ name: 'equiposZonas' })
  @Type(() => Equipo)
  public equipos: Equipo[];
  public fase: Fase;

  constructor(idZona: number, descripcion: string, torneo: Torneo, equipos: Equipo[], fase: Fase) {
    this.idZona = idZona;
    this.descripcion = descripcion;
    this.torneo = torneo;
    this.equipos = equipos;
    this.fase = fase;
  }
}
