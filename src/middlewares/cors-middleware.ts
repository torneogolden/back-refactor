import cors = require('cors');
import { RequestHandler } from 'express';

export const corsMiddleware = (): RequestHandler => {
  return cors({
    credentials: true,
    origin: [process.env.URL_CLIENT_WEB, process.env.URL_CLIENT_WEB2],
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    exposedHeaders: ['Authorization']
  });
};
