export * from './entities/Arbitros';
export * from './entities/Canchas';
export * from './entities/CategoriaEquipos';
export * from './entities/Categorias';
export * from './entities/CategoriasNoticias';
export * from './entities/Clubes';
export * from './entities/Contactos';
export * from './entities/Domicilios';
export * from './entities/Equipos';
export * from './entities/EquiposZona';
export * from './entities/EstadoFecha';
export * from './entities/EstadosCivil';
export * from './entities/EstadosPartidos';
export * from './entities/EstadosTorneo';
export * from './entities/EtapaPlayoff';
export * from './entities/Fases';
export * from './entities/Fechas';
export * from './entities/Files';
export * from './entities/Fixture';
export * from './entities/FixtureZona';
export * from './entities/Fotos';
export * from './entities/Goleadores';
export * from './entities/Goles';
export * from './entities/HorariosFijos';
export * from './entities/Jugadores';
export * from './entities/Llaves';
export * from './entities/Localidades';
export * from './entities/Logs';
export * from './entities/Modalidades';
export * from './entities/Nacionalidades';
export * from './entities/Noticias';
export * from './entities/Partidos';
export * from './entities/Perfiles';
export * from './entities/Permisos';
export * from './entities/Personas';
export * from './entities/Playoff';
export * from './entities/Posiciones';
export * from './entities/PosicionesZona';
export * from './entities/Provincias';
export * from './entities/Reglamentos';
export * from './entities/ReglasTorneo';
export * from './entities/RepresentanteEquipo';
export * from './entities/Restricciones';
export * from './entities/Resultados';
export * from './entities/ResultadosZona';
export * from './entities/Sanciones';
export * from './entities/SancionesEquipo';
export * from './entities/SancionesTorneo';
export * from './entities/Sponsors';
export * from './entities/TiposCancha';
export * from './entities/TiposDocumento';
export * from './entities/TiposFixture';
export * from './entities/TiposSanciones';
export * from './entities/TiposTorneos';
export * from './entities/Torneos';
export * from './entities/Turnos';
export * from './entities/TurnosFixture';
export * from './entities/Usuarios';
export * from './entities/Veedores';
export * from './entities/Zonas';
export * from './entities/TiposArchivo';
