import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Clubes } from './Clubes';
import { Equipos } from './Equipos';
import { Fechas } from './Fechas';
import { Torneos } from './Torneos';

@Index('fk_foto_torneo_idx', ['idTorneo'], {})
@Index('fk_foto_club_idx', ['idClub'], {})
@Index('fk_foto_equipo_idx', ['idEquipo'], {})
@Index('fk_foto_fecha_idx', ['idFecha'], {})
@Entity('fotos', { schema: 'torneo' })
export class Fotos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_foto' })
  idFoto: number;

  @Column('blob', { name: 'foto', nullable: true })
  foto: Buffer | null;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @Column('int', { name: 'id_club', nullable: true })
  idClub: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_fecha', nullable: true })
  idFecha: number | null;

  @ManyToOne(() => Clubes, (clubes) => clubes.fotos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_club', referencedColumnName: 'idClub' }])
  club: Clubes;

  @ManyToOne(() => Equipos, (equipos) => equipos.fotos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;

  @ManyToOne(() => Fechas, (fechas) => fechas.fotos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fecha', referencedColumnName: 'idFecha' }])
  fecha: Fechas;

  @ManyToOne(() => Torneos, (torneos) => torneos.fotos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;
}
