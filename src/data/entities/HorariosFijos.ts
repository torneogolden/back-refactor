import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Turnos } from './Turnos';
import { Partidos } from './Partidos';

@Index('fk_horario_turno_idx', ['idTurno'], {})
@Entity('horarios_fijos', { schema: 'torneo' })
export class HorariosFijos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_horario' })
  idHorario: number;

  @Column('int', { name: 'id_turno', nullable: true })
  idTurno: number | null;

  @Column('varchar', { name: 'inicio', nullable: true, length: 50 })
  inicio: string | null;

  @Column('varchar', { name: 'fin', nullable: true, length: 50 })
  fin: string | null;

  @ManyToOne(() => Turnos, (turnos) => turnos.horariosFijos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_turno', referencedColumnName: 'id' }])
  turno: Turnos;

  @OneToMany(() => Partidos, (partidos) => partidos.horario)
  partidos: Partidos[];
}
