import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { EstadoFecha } from './EstadoFecha';
import { FixtureZona } from './FixtureZona';
import { Fases } from './Fases';
import { Fixture } from './Fixture';
import { Fotos } from './Fotos';
import { Noticias } from './Noticias';
import { Partidos } from './Partidos';
import { Sanciones } from './Sanciones';

@Index('fk_estado_fecha_idx', ['idEstado'], {})
@Index('fk_fecha_fixture_idx', ['idFixture'], {})
@Index('fk_zona_fixture_idx', ['idFixtureZona'], {})
@Index('fk_fecha_fecha_interzonal_idx', ['idFase'], {})
@Entity('fechas', { schema: 'torneo' })
export class Fechas {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_fecha' })
  idFecha: number;

  @Column('date', { name: 'fecha', nullable: true })
  fecha: string | null;

  @Column('int', { name: 'id_estado', nullable: true })
  idEstado: number | null;

  @Column('int', { name: 'id_fixture', nullable: true })
  idFixture: number | null;

  @Column('int', { name: 'id_fixture_zona', nullable: true })
  idFixtureZona: number | null;

  @Column('int', { name: 'id_fase', nullable: true })
  idFase: number | null;

  @ManyToOne(() => EstadoFecha, (estadoFecha) => estadoFecha.fechas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_estado', referencedColumnName: 'idEstado' }])
  estado: EstadoFecha;

  @ManyToOne(() => FixtureZona, (fixtureZona) => fixtureZona.fechas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fixture_zona', referencedColumnName: 'idFixture' }])
  fixtureZona: FixtureZona;

  @ManyToOne(() => Fases, (fases) => fases.fechas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fase', referencedColumnName: 'idFase' }])
  fase: Fases;

  @ManyToOne(() => Fixture, (fixture) => fixture.fechas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fixture', referencedColumnName: 'idFixture' }])
  fixture: Fixture;

  @OneToMany(() => Fotos, (fotos) => fotos.fecha)
  fotos: Fotos[];

  @OneToMany(() => Noticias, (noticias) => noticias.fecha)
  noticias: Noticias[];

  @OneToMany(() => Partidos, (partidos) => partidos.fecha)
  partidos: Partidos[];

  @OneToMany(() => Sanciones, (sanciones) => sanciones.fechaInicioB)
  sanciones: Sanciones[];

  @OneToMany(() => Sanciones, (sanciones) => sanciones.fechaFinB)
  sancionesB: Sanciones[];
}
