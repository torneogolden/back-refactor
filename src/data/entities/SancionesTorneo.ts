import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Torneos } from './Torneos';
import { Jugadores } from './Jugadores';

@Index('fk_jugador_sanciones_idx', ['idJugador'], {})
@Index('fk_jugador_sanciones_torneo_idx', ['idTorneo'], {})
@Entity('sanciones_torneo', { schema: 'torneo' })
export class SancionesTorneo {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_sancion' })
  idSancion: number;

  @Column('int', { name: 'id_jugador', nullable: true })
  idJugador: number | null;

  @Column('int', { name: 'cantidad_amarillas', nullable: true })
  cantidadAmarillas: number | null;

  @Column('int', { name: 'cantidad_rojas', nullable: true })
  cantidadRojas: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @ManyToOne(() => Torneos, (torneos) => torneos.sancionesTorneos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Jugadores, (jugadores) => jugadores.sancionesTorneos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_jugador', referencedColumnName: 'idJugador' }])
  jugador: Jugadores;
}
