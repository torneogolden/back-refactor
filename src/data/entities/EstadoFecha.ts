import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Fechas } from './Fechas';

@Entity('estado_fecha', { schema: 'torneo' })
export class EstadoFecha {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_estado' })
  idEstado: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 200 })
  descripcion: string | null;

  @OneToMany(() => Fechas, (fechas) => fechas.estado)
  fechas: Fechas[];
}
