import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Equipos } from './Equipos';
import { TiposArchivo } from './TiposArchivo';
import { Noticias } from './Noticias';
import { Personas } from './Personas';

@Index('fk_archivo_equipo_idx', ['idEquipo'], {})
@Index('fk_archivo_tipo_idx', ['idTipoArchivo'], {})
@Entity('files', { schema: 'torneo' })
export class Files {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'name', length: 200 })
  name: string;

  @Column('varchar', { name: 'imagePath', length: 200 })
  imagePath: string;

  @Column('varchar', { name: 'thumbPath', length: 200 })
  thumbPath: string;

  @Column('varchar', { name: 'idProyecto', length: 200 })
  idProyecto: string;

  @Column('varchar', { name: 'idSeccion', length: 200 })
  idSeccion: string;

  @Column('int', { name: 'fileSize' })
  fileSize: number;

  @Column('int', { name: 'idEquipo', nullable: true })
  idEquipo: number | null;

  @Column('int', { name: 'idTipoArchivo', nullable: true })
  idTipoArchivo: number | null;

  @ManyToOne(() => Equipos, (equipos) => equipos.files, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'idEquipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;

  @ManyToOne(() => TiposArchivo, (tiposArchivo) => tiposArchivo.files, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'idTipoArchivo', referencedColumnName: 'id' }])
  tipoArchivo: TiposArchivo;

  @OneToMany(() => Noticias, (noticias) => noticias.thumbnail)
  noticias: Noticias[];

  @OneToMany(() => Personas, (personas) => personas.foto)
  personas: Personas[];
}
