import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TiposSanciones } from './TiposSanciones';

@Entity('restricciones', { schema: 'torneo' })
export class Restricciones {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_restriccion' })
  idRestriccion: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => TiposSanciones, (tiposSanciones) => tiposSanciones.restriccion)
  tiposSanciones: TiposSanciones[];
}
