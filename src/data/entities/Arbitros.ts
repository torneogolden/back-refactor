import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Personas } from './Personas';
import { Partidos } from './Partidos';

@Index('fk_arbitro_persona_idx', ['idPersona'], {})
@Entity('arbitros', { schema: 'torneo' })
export class Arbitros {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_arbitro' })
  idArbitro: number;

  @Column('int', { name: 'matricula', nullable: true })
  matricula: number | null;

  @Column('int', { name: 'id_persona', nullable: true })
  idPersona: number | null;

  @ManyToOne(() => Personas, (personas) => personas.arbitros, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_persona', referencedColumnName: 'idPersona' }])
  persona: Personas;

  @OneToMany(() => Partidos, (partidos) => partidos.arbitro)
  partidos: Partidos[];
}
