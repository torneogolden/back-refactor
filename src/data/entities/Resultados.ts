import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Partidos } from './Partidos';
import { Equipos } from './Equipos';

@Index('fk_ganador_equipo_idx', ['idGanador'], {})
@Index('fk_perdedor_equipo_idx', ['idPerdedor'], {})
@Entity('resultados', { schema: 'torneo' })
export class Resultados {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_resultado' })
  idResultado: number;

  @Column('int', { name: 'id_ganador', nullable: true })
  idGanador: number | null;

  @Column('int', { name: 'id_perdedor', nullable: true })
  idPerdedor: number | null;

  @Column('tinyint', { name: 'empate', nullable: true })
  empate: number | null;

  @OneToMany(() => Partidos, (partidos) => partidos.resultado)
  partidos: Partidos[];

  @ManyToOne(() => Equipos, (equipos) => equipos.resultados, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_perdedor', referencedColumnName: 'idEquipo' }])
  perdedor: Equipos;

  @ManyToOne(() => Equipos, (equipos) => equipos.resultadosB, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_ganador', referencedColumnName: 'idEquipo' }])
  ganador: Equipos;
}
