import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Canchas } from './Canchas';

@Index('fk_cancha_tipo_idx', ['idCancha'], {})
@Entity('tipos_cancha', { schema: 'torneo' })
export class TiposCancha {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_tipo' })
  idTipo: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 200 })
  descripcion: string | null;

  @Column('int', { name: 'id_cancha', nullable: true })
  idCancha: number | null;

  @ManyToOne(() => Canchas, (canchas) => canchas.tiposCanchas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_cancha', referencedColumnName: 'idCancha' }])
  cancha: Canchas;
}
