import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Equipos } from './Equipos';
import { EquiposZona } from './EquiposZona';
import { Fixture } from './Fixture';
import { FixtureZona } from './FixtureZona';
import { Fotos } from './Fotos';
import { Goleadores } from './Goleadores';
import { Goles } from './Goles';
import { Noticias } from './Noticias';
import { Posiciones } from './Posiciones';
import { PosicionesZona } from './PosicionesZona';
import { ReglasTorneo } from './ReglasTorneo';
import { Sanciones } from './Sanciones';
import { SancionesTorneo } from './SancionesTorneo';
import { Sponsors } from './Sponsors';
import { Categorias } from './Categorias';
import { TiposTorneos } from './TiposTorneos';
import { Modalidades } from './Modalidades';
import { Fases } from './Fases';
import { Zonas } from './Zonas';

@Index('fk_tipo_torneo_idx', ['idTipo'], {})
@Index('fk_categoria_torneo_idx', ['idCategoria'], {})
@Index('fk_regla_torneo_idx', ['idRegla'], {})
@Index('fk_modalidad_torneo_idx', ['idModalidad'], {})
@Index('fk_torneo_fase_idx', ['idFase'], {})
@Index('fk_estado_torneo_idx', ['idEstado'], {})
@Entity('torneos', { schema: 'torneo' })
export class Torneos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_torneo' })
  idTorneo: number;

  @Column('varchar', { name: 'nombre', nullable: true, length: 45 })
  nombre: string | null;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @Column('date', { name: 'fecha_inicio', nullable: true })
  fechaInicio: string | null;

  @Column('date', { name: 'fecha_fin', nullable: true })
  fechaFin: string | null;

  @Column('int', { name: 'id_tipo', nullable: true })
  idTipo: number | null;

  @Column('int', { name: 'id_categoria', nullable: true })
  idCategoria: number | null;

  @Column('int', { name: 'id_regla', nullable: true })
  idRegla: number | null;

  @Column('int', { name: 'id_modalidad', nullable: true })
  idModalidad: number | null;

  @Column('int', { name: 'id_fase' })
  idFase: number;

  @Column('int', { name: 'id_estado', nullable: true })
  idEstado: number | null;

  @OneToMany(() => Equipos, (equipos) => equipos.torneo)
  equipos: Equipos[];

  @OneToMany(() => EquiposZona, (equiposZona) => equiposZona.torneo)
  equiposZonas: EquiposZona[];

  @OneToMany(() => Fixture, (fixture) => fixture.torneo)
  fixtures: Fixture[];

  @OneToMany(() => FixtureZona, (fixtureZona) => fixtureZona.torneo)
  fixtureZonas: FixtureZona[];

  @OneToMany(() => Fotos, (fotos) => fotos.torneo)
  fotos: Fotos[];

  @OneToMany(() => Goleadores, (goleadores) => goleadores.torneo)
  goleadores: Goleadores[];

  @OneToMany(() => Goles, (goles) => goles.torneo)
  goles: Goles[];

  @OneToMany(() => Noticias, (noticias) => noticias.torneo)
  noticias: Noticias[];

  @OneToMany(() => Posiciones, (posiciones) => posiciones.torneo)
  posiciones: Posiciones[];

  @OneToMany(() => PosicionesZona, (posicionesZona) => posicionesZona.torneo)
  posicionesZonas: PosicionesZona[];

  @OneToMany(() => ReglasTorneo, (reglasTorneo) => reglasTorneo.torneo)
  reglasTorneos: ReglasTorneo[];

  @OneToMany(() => Sanciones, (sanciones) => sanciones.torneo)
  sanciones: Sanciones[];

  @OneToMany(() => SancionesTorneo, (sancionesTorneo) => sancionesTorneo.torneo)
  sancionesTorneos: SancionesTorneo[];

  @OneToMany(() => Sponsors, (sponsors) => sponsors.torneo)
  sponsors: Sponsors[];

  @ManyToOne(() => Categorias, (categorias) => categorias.torneos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_categoria', referencedColumnName: 'idCategoria' }])
  categoria: Categorias;

  @ManyToOne(() => TiposTorneos, (tiposTorneos) => tiposTorneos.torneos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_tipo', referencedColumnName: 'idTipo' }])
  tipo: TiposTorneos;

  @ManyToOne(() => ReglasTorneo, (reglasTorneo) => reglasTorneo.torneos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_regla', referencedColumnName: 'idRegla' }])
  regla: ReglasTorneo;

  @ManyToOne(() => Modalidades, (modalidades) => modalidades.torneos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_modalidad', referencedColumnName: 'idModalidad' }])
  modalidad: Modalidades;

  @ManyToOne(() => Fases, (fases) => fases.torneos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fase', referencedColumnName: 'idFase' }])
  fase: Fases;

  @OneToMany(() => Zonas, (zonas) => zonas.torneo)
  zonas: Zonas[];
}
