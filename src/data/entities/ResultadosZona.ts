import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Partidos } from './Partidos';
import { Equipos } from './Equipos';

@Index('fk_ganador_equipo_idx', ['idGanador'], {})
@Index('fk_perdedor_equipo_idx', ['idPerdedor'], {})
@Index('fk_resultado_zona_idx', ['idZona'], {})
@Entity('resultados_zona', { schema: 'torneo' })
export class ResultadosZona {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_resultado' })
  idResultado: number;

  @Column('int', { name: 'id_ganador', nullable: true })
  idGanador: number | null;

  @Column('int', { name: 'id_perdedor', nullable: true })
  idPerdedor: number | null;

  @Column('tinyint', { name: 'empate', nullable: true })
  empate: number | null;

  @Column('int', { name: 'id_zona', nullable: true })
  idZona: number | null;

  @OneToMany(() => Partidos, (partidos) => partidos.resultadosZona)
  partidos: Partidos[];

  @ManyToOne(() => Equipos, (equipos) => equipos.resultadosZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_ganador', referencedColumnName: 'idEquipo' }])
  ganador: Equipos;

  @ManyToOne(() => Equipos, (equipos) => equipos.resultadosZonasB, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_perdedor', referencedColumnName: 'idEquipo' }])
  perdedor: Equipos;
}
