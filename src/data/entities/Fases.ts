import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Fechas } from './Fechas';
import { Sanciones } from './Sanciones';
import { Torneos } from './Torneos';
import { Zonas } from './Zonas';

@Entity('fases', { schema: 'torneo' })
export class Fases {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_fase' })
  idFase: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => Fechas, (fechas) => fechas.fase)
  fechas: Fechas[];

  @OneToMany(() => Sanciones, (sanciones) => sanciones.fase)
  sanciones: Sanciones[];

  @OneToMany(() => Torneos, (torneos) => torneos.fase)
  torneos: Torneos[];

  @OneToMany(() => Zonas, (zonas) => zonas.fase)
  zonas: Zonas[];
}
