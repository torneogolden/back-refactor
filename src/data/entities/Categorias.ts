import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Equipos } from './Equipos';
import { Torneos } from './Torneos';

@Entity('categorias', { schema: 'torneo' })
export class Categorias {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_categoria' })
  idCategoria: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => Equipos, (equipos) => equipos.categoriaEquipo)
  equipos: Equipos[];

  @OneToMany(() => Torneos, (torneos) => torneos.categoria)
  torneos: Torneos[];
}
