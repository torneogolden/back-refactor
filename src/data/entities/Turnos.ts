import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { HorariosFijos } from './HorariosFijos';

@Entity('turnos', { schema: 'torneo' })
export class Turnos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => HorariosFijos, (horariosFijos) => horariosFijos.turno)
  horariosFijos: HorariosFijos[];
}
