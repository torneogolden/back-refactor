import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Goleadores } from './Goleadores';
import { Goles } from './Goles';
import { Equipos } from './Equipos';
import { Personas } from './Personas';
import { Sanciones } from './Sanciones';
import { SancionesTorneo } from './SancionesTorneo';

@Index('fk_jugador_persona_idx', ['idPersona'], {})
@Index('fk_jugador_equipo_idx', ['idEquipo'], {})
@Entity('jugadores', { schema: 'torneo' })
export class Jugadores {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_jugador' })
  idJugador: number;

  @Column('int', { name: 'numero', nullable: true })
  numero: number | null;

  @Column('date', { name: 'fecha_alta', nullable: true })
  fechaAlta: string | null;

  @Column('int', { name: 'id_persona', nullable: true })
  idPersona: number | null;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @Column('varchar', { name: 'rol', nullable: true, length: 45 })
  rol: string | null;

  nombre: string | null;
  apellido: string | null;

  @OneToMany(() => Goleadores, (goleadores) => goleadores.jugador)
  goleadores: Goleadores[];

  @OneToMany(() => Goles, (goles) => goles.jugador)
  goles: Goles[];

  @ManyToOne(() => Equipos, (equipos) => equipos.jugadores, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;

  @ManyToOne(() => Personas, (personas) => personas.jugadores, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_persona', referencedColumnName: 'idPersona' }])
  persona: Personas;

  @OneToMany(() => Sanciones, (sanciones) => sanciones.jugador)
  sanciones: Sanciones[];

  @OneToMany(() => SancionesTorneo, (sancionesTorneo) => sancionesTorneo.jugador)
  sancionesTorneos: SancionesTorneo[];
}
