import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Torneos } from './Torneos';

@Entity('modalidades', { schema: 'torneo' })
export class Modalidades {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_modalidad' })
  idModalidad: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => Torneos, (torneos) => torneos.modalidad)
  torneos: Torneos[];
}
