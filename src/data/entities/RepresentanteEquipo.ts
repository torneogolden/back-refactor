import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Usuarios } from './Usuarios';
import { Equipos } from './Equipos';

@Index('fk_representante_usuario_idx', ['idUsuario'], {})
@Index('fk_representante_equipo_idx', ['idEquipo'], {})
@Entity('representante_equipo', { schema: 'torneo' })
export class RepresentanteEquipo {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('int', { name: 'id_usuario', nullable: true })
  idUsuario: number | null;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @ManyToOne(() => Usuarios, (usuarios) => usuarios.representanteEquipos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_usuario', referencedColumnName: 'idUsuario' }])
  usuario: Usuarios;

  @ManyToOne(() => Equipos, (equipos) => equipos.representanteEquipos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;
}
