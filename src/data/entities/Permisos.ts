import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Perfiles } from './Perfiles';

@Index('fk_perfil_persmiso_idx', ['idPerfil'], {})
@Entity('permisos', { schema: 'torneo' })
export class Permisos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_permiso' })
  idPermiso: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @Column('date', { name: 'fechaExpiracion', nullable: true })
  fechaExpiracion: string | null;

  @Column('int', { name: 'id_perfil', nullable: true })
  idPerfil: number | null;

  @ManyToOne(() => Perfiles, (perfiles) => perfiles.permisos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_perfil', referencedColumnName: 'idPerfil' }])
  perfil: Perfiles;
}
