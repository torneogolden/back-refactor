import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Files } from './Files';
import { Torneos } from './Torneos';
import { CategoriasNoticias } from './CategoriasNoticias';
import { Clubes } from './Clubes';
import { Fechas } from './Fechas';

@Index('fk_noticia_torneo_idx', ['idTorneo'], {})
@Index('fk_noticia_club_idx', ['idClub'], {})
@Index('fk_noticia_fecha_idx', ['idFecha'], {})
@Index('fk_categoria_noticia_idx', ['idCategoriaNoticia'], {})
@Index('fk_noticia_thumbnail_idx', ['idThumbnail'], {})
@Entity('noticias', { schema: 'torneo' })
export class Noticias {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_noticia' })
  idNoticia: number;

  @Column('varchar', { name: 'titulo', nullable: true, length: 200 })
  titulo: string | null;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 10000 })
  descripcion: string | null;

  @Column('date', { name: 'fecha', nullable: true })
  fecha: string | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_club', nullable: true })
  idClub: number | null;

  @Column('int', { name: 'id_fecha', nullable: true })
  idFecha: number | null;

  @Column('int', { name: 'id_categoria_noticia', nullable: true })
  idCategoriaNoticia: number | null;

  @Column('varchar', { name: 'tags', nullable: true, length: 100 })
  tags: string | null;

  @Column('int', { name: 'id_thumbnail', nullable: true })
  idThumbnail: number | null;

  @ManyToOne(() => Files, (files) => files.noticias, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_thumbnail', referencedColumnName: 'id' }])
  thumbnail: Files;

  @ManyToOne(() => Torneos, (torneos) => torneos.noticias, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => CategoriasNoticias, (categoriasNoticias) => categoriasNoticias.noticias, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
  @JoinColumn([
    {
      name: 'id_categoria_noticia',
      referencedColumnName: 'idCategoriaNoticia'
    }
  ])
  categoriaNoticia: CategoriasNoticias;

  @ManyToOne(() => Clubes, (clubes) => clubes.noticias, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_club', referencedColumnName: 'idClub' }])
  club: Clubes;

  @ManyToOne(() => Fechas, (fechas) => fechas.noticias, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fecha', referencedColumnName: 'idFecha' }])
  fechaB: Fechas;
}
