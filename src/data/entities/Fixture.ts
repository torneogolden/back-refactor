import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Fechas } from './Fechas';
import { TiposFixture } from './TiposFixture';
import { Torneos } from './Torneos';

@Index('fk_fixture_torneo_idx', ['idTorneo'], {})
@Index('fk_fixture_tipo_idx', ['idTipo'], {})
@Entity('fixture', { schema: 'torneo' })
export class Fixture {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_fixture' })
  idFixture: number;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_tipo', nullable: true })
  idTipo: number | null;

  @OneToMany(() => Fechas, (fechas) => fechas.fixture)
  fechas: Fechas[];

  @ManyToOne(() => TiposFixture, (tiposFixture) => tiposFixture.fixtures, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_tipo', referencedColumnName: 'idTipo' }])
  tipo: TiposFixture;

  @ManyToOne(() => Torneos, (torneos) => torneos.fixtures, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;
}
