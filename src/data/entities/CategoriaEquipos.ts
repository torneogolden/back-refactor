import { Column, Entity } from 'typeorm';

@Entity('categoria_equipos', { schema: 'torneo' })
export class CategoriaEquipos {
  @Column('int', { primary: true, name: 'id' })
  id: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;
}
