import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Sanciones } from './Sanciones';
import { Restricciones } from './Restricciones';

@Index('fk_tipo_restriccion_idx', ['idRestriccion'], {})
@Entity('tipos_sanciones', { schema: 'torneo' })
export class TiposSanciones {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_tipo' })
  idTipo: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @Column('int', { name: 'id_restriccion', nullable: true })
  idRestriccion: number | null;

  @OneToMany(() => Sanciones, (sanciones) => sanciones.tipo)
  sanciones: Sanciones[];

  @ManyToOne(() => Restricciones, (restricciones) => restricciones.tiposSanciones, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
  @JoinColumn([{ name: 'id_restriccion', referencedColumnName: 'idRestriccion' }])
  restriccion: Restricciones;
}
