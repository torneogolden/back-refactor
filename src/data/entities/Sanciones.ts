import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TiposSanciones } from './TiposSanciones';
import { Partidos } from './Partidos';
import { Zonas } from './Zonas';
import { Torneos } from './Torneos';
import { Fechas } from './Fechas';
import { Jugadores } from './Jugadores';
import { Fases } from './Fases';

@Index('fk_jugador_sancion_idx', ['idJugador'], {})
@Index('fk_tipo_sancion_idx', ['idTipo'], {})
@Index('fk_fecha_inicio_fechas_idx', ['fechaInicio'], {})
@Index('fk_fecha_fin_fechas_idx', ['fechaFin'], {})
@Index('fk_id_partido_idx', ['idPartido'], {})
@Index('fk_zona_sancion_idx', ['idZona'], {})
@Index('fk_sancion_fase_idx', ['idFase'], {})
@Index('fk_sancion_torneo_idx', ['idTorneo'], {})
@Index('fk_equipo_sancion_idx', ['idEquipo'], {})
@Entity('sanciones', { schema: 'torneo' })
export class Sanciones {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_sancion' })
  idSancion: number;

  @Column('int', { name: 'id_jugador', nullable: true })
  idJugador: number | null;

  @Column('int', { name: 'id_tipo', nullable: true })
  idTipo: number | null;

  @Column('int', { name: 'fecha_inicio', nullable: true })
  fechaInicio: number | null;

  @Column('int', { name: 'fecha_fin', nullable: true })
  fechaFin: number | null;

  @Column('int', { name: 'id_partido', nullable: true })
  idPartido: number | null;

  @Column('varchar', { name: 'detalle', nullable: true, length: 300 })
  detalle: string | null;

  @Column('int', { name: 'id_zona', nullable: true })
  idZona: number | null;

  @Column('int', { name: 'id_fase', nullable: true })
  idFase: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @ManyToOne(() => TiposSanciones, (tiposSanciones) => tiposSanciones.sanciones, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
  @JoinColumn([{ name: 'id_tipo', referencedColumnName: 'idTipo' }])
  tipo: TiposSanciones;

  @ManyToOne(() => Partidos, (partidos) => partidos.sanciones, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_partido', referencedColumnName: 'idPartido' }])
  partido: Partidos;

  @ManyToOne(() => Zonas, (zonas) => zonas.sanciones, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_zona', referencedColumnName: 'idZona' }])
  zona: Zonas;

  @ManyToOne(() => Torneos, (torneos) => torneos.sanciones, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Fechas, (fechas) => fechas.sanciones, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'fecha_inicio', referencedColumnName: 'idFecha' }])
  fechaInicioB: Fechas;

  @ManyToOne(() => Fechas, (fechas) => fechas.sancionesB, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'fecha_fin', referencedColumnName: 'idFecha' }])
  fechaFinB: Fechas;

  @ManyToOne(() => Jugadores, (jugadores) => jugadores.sanciones, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_jugador', referencedColumnName: 'idJugador' }])
  jugador: Jugadores;

  @ManyToOne(() => Fases, (fases) => fases.sanciones, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fase', referencedColumnName: 'idFase' }])
  fase: Fases;
}
