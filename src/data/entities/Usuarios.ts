import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { RepresentanteEquipo } from './RepresentanteEquipo';
import { Perfiles } from './Perfiles';

@Index('fk_usuario_perfil_idx', ['idPerfil'], {})
@Entity('usuarios', { schema: 'torneo' })
export class Usuarios {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_usuario' })
  idUsuario: number;

  @Column('varchar', { name: 'n_usuario', length: 45 })
  nUsuario: string;

  @Column('varchar', { name: 'password', length: 45 })
  password: string;

  @Column('int', { name: 'id_perfil', nullable: true })
  idPerfil: number | null;

  @Column('datetime', {
    name: 'fecha_alta',
    default: () => 'CURRENT_TIMESTAMP'
  })
  fechaAlta: Date;

  @Column('date', { name: 'caducidad', nullable: true })
  caducidad: string | null;

  @OneToMany(() => RepresentanteEquipo, (representanteEquipo) => representanteEquipo.usuario)
  representanteEquipos: RepresentanteEquipo[];

  @ManyToOne(() => Perfiles, (perfiles) => perfiles.usuarios, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_perfil', referencedColumnName: 'idPerfil' }])
  perfil: Perfiles;
}
