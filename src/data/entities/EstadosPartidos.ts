import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Partidos } from './Partidos';

@Entity('estados_partidos', { schema: 'torneo' })
export class EstadosPartidos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_estado' })
  idEstado: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => Partidos, (partidos) => partidos.estadoPartido)
  partidos: Partidos[];
}
