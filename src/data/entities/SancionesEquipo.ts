import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Equipos, Torneos, Zonas } from '..';

@Index('fk_sanciones_equipo_equipo_idx', ['idEquipo'], {})
@Index('fk_sanciones_equipo_zona_idx', ['idZona'], {})
@Index('fk_sanciones_equipo_torneo_idx', ['idTorneo'], {})
@Entity('sanciones_equipo', { schema: 'torneo' })
export class SancionesEquipo {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_sancion_equipo' })
  idSancionEquipo: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 200 })
  descripcion: string | null;

  @Column('int', { name: 'puntos_restados', nullable: true })
  puntosRestados: number | null;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @Column('int', { name: 'id_zona', nullable: true })
  idZona: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'fair_play', nullable: true })
  fairPlay: number | null;
  @ManyToOne(() => Equipos)
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;

  @ManyToOne(() => Torneos)
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Zonas)
  @JoinColumn([{ name: 'id_zona', referencedColumnName: 'idZona' }])
  zona: Zonas;
}
