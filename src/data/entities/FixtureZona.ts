import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Fechas } from './Fechas';
import { TiposFixture } from './TiposFixture';
import { Torneos } from './Torneos';
import { Zonas } from './Zonas';

@Index('fk_fixture_torneo_idx', ['idTorneo'], {})
@Index('fk_tipo_fixtureZ_idx', ['idTipo'], {})
@Index('fk_zona_fixture_idx', ['idZona'], {})
@Entity('fixture_zona', { schema: 'torneo' })
export class FixtureZona {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_fixture' })
  idFixture: number;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_tipo', nullable: true })
  idTipo: number | null;

  @Column('int', { name: 'id_zona', nullable: true })
  idZona: number | null;

  @OneToMany(() => Fechas, (fechas) => fechas.fixtureZona)
  fechas: Fechas[];

  @ManyToOne(() => TiposFixture, (tiposFixture) => tiposFixture.fixtureZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_tipo', referencedColumnName: 'idTipo' }])
  tipo: TiposFixture;

  @ManyToOne(() => Torneos, (torneos) => torneos.fixtureZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Zonas, (zonas) => zonas.fixtureZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_zona', referencedColumnName: 'idZona' }])
  zona: Zonas;
}
