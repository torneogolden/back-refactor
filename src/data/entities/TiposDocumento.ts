import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Personas } from './Personas';

@Entity('tipos_documento', { schema: 'torneo' })
export class TiposDocumento {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_tipo_documento' })
  idTipoDocumento: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => Personas, (personas) => personas.tipoDocumento)
  personas: Personas[];
}
