import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('etapa_playoff', { schema: 'torneo' })
export class EtapaPlayoff {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_etapa' })
  idEtapa: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;
}
