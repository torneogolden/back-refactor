import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('llaves', { schema: 'torneo' })
export class Llaves {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_llave' })
  idLlave: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;
}
