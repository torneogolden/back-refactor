import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('fk_turno_cancha_idx', ['idCancha'], {})
@Index('fk_turno_horario_idx', ['idHorario'], {})
@Entity('turnos_fixture', { schema: 'torneo' })
export class TurnosFixture {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_turno_fixture' })
  idTurnoFixture: number;

  @Column('int', { name: 'id_cancha', nullable: true })
  idCancha: number | null;

  @Column('int', { name: 'id_horario', nullable: true })
  idHorario: number | null;
}
