import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Torneos } from './Torneos';

@Entity('tipos_torneos', { schema: 'torneo' })
export class TiposTorneos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_tipo' })
  idTipo: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => Torneos, (torneos) => torneos.tipo)
  torneos: Torneos[];
}
