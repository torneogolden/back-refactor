import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('estados_torneo', { schema: 'torneo' })
export class EstadosTorneo {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_estado' })
  idEstado: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;
}
