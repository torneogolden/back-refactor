import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Permisos } from './Permisos';
import { Usuarios } from './Usuarios';

@Index('n_perfil_UNIQUE', ['nPerfil'], { unique: true })
@Index('IDX_78a8Bbbac73463Bbbde', ['nPerfil'], { unique: true })
@Entity('perfiles', { schema: 'torneo' })
export class Perfiles {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_perfil' })
  idPerfil: number;

  @Column('varchar', { name: 'n_perfil', unique: true, length: 45 })
  nPerfil: string;

  @OneToMany(() => Permisos, (permisos) => permisos.perfil)
  permisos: Permisos[];

  @OneToMany(() => Usuarios, (usuarios) => usuarios.perfil)
  usuarios: Usuarios[];
}
