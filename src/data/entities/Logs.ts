import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('logs', { schema: 'torneo' })
export class Logs {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'clase', nullable: true, length: 1000 })
  clase: string | null;

  @Column('varchar', { name: 'metodo', nullable: true, length: 1000 })
  metodo: string | null;

  @Column('varchar', { name: 'excepcion', nullable: true, length: 1000 })
  excepcion: string | null;

  @Column('datetime', { name: 'fecha', nullable: true })
  fecha: Date | null;
}
