import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Goles } from './Goles';
import { Fechas } from './Fechas';
import { HorariosFijos } from './HorariosFijos';
import { Equipos } from './Equipos';
import { EstadosPartidos } from './EstadosPartidos';
import { ResultadosZona } from './ResultadosZona';
import { Resultados } from './Resultados';
import { Canchas } from './Canchas';
import { Arbitros } from './Arbitros';
import { Veedores } from './Veedores';
import { Sanciones } from './Sanciones';

@Index('fk_fecha_partido_idx', ['idFecha'], {})
@Index('fk_local_partido_idx', ['local'], {})
@Index('fk_visitante_partido_idx', ['visitante'], {})
@Index('fk_arbitro_partido_idx', ['idArbitro'], {})
@Index('fk_estado_partido_idx', ['idEstadoPartido'], {})
@Index('fk_cancha_partido_idx', ['idCancha'], {})
@Index('fk_veedor_partido_idx', ['idVeedor'], {})
@Index('fk_resultado_partido_idx', ['idResultado'], {})
@Index('fk_resultado_partido_zona_idx', ['idResultadosZona'], {})
@Index('fk_horario_fijo_partido_idx', ['idHorarioFijo'], {})
@Entity('partidos', { schema: 'torneo' })
export class Partidos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_partido' })
  idPartido: number;

  @Column('int', { name: 'duracion', nullable: true })
  duracion: number | null;

  @Column('int', { name: 'id_fecha', nullable: true })
  idFecha: number | null;

  @Column('varchar', { name: 'hora_inicio', nullable: true, length: 50 })
  horaInicio: string | null;

  @Column('varchar', { name: 'hora_fin', nullable: true, length: 50 })
  horaFin: string | null;

  @Column('int', { name: 'local', nullable: true })
  local: number | null;

  @Column('int', { name: 'visitante', nullable: true })
  visitante: number | null;

  @Column('int', { name: 'id_arbitro', nullable: true })
  idArbitro: number | null;

  @Column('int', { name: 'id_estado_partido', nullable: true })
  idEstadoPartido: number | null;

  @Column('int', { name: 'id_cancha', nullable: true })
  idCancha: number | null;

  @Column('int', { name: 'id_veedor', nullable: true })
  idVeedor: number | null;

  @Column('int', { name: 'id_resultados_zona', nullable: true })
  idResultadosZona: number | null;

  @Column('int', { name: 'id_resultado', nullable: true })
  idResultado: number | null;

  @Column('int', { name: 'id_horario_fijo', nullable: true })
  idHorarioFijo: number | null;

  @Column('tinyint', { name: 'esInterzonal', nullable: true })
  esInterzonal: number | null;

  @OneToMany(() => Goles, (goles) => goles.partido)
  goles: Goles[];

  @ManyToOne(() => Fechas, (fechas) => fechas.partidos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fecha', referencedColumnName: 'idFecha' }])
  fecha: Fechas;

  @ManyToOne(() => HorariosFijos, (horariosFijos) => horariosFijos.partidos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_horario_fijo', referencedColumnName: 'idHorario' }])
  horario: HorariosFijos;

  @ManyToOne(() => Equipos, (equipos) => equipos.partidos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'local', referencedColumnName: 'idEquipo' }])
  localB: Equipos;

  @ManyToOne(() => Equipos, (equipos) => equipos.partidosB, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'visitante', referencedColumnName: 'idEquipo' }])
  visitanteB: Equipos;

  @ManyToOne(() => EstadosPartidos, (estadosPartidos) => estadosPartidos.partidos, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
  @JoinColumn([{ name: 'id_estado_partido', referencedColumnName: 'idEstado' }])
  estadoPartido: EstadosPartidos;

  @ManyToOne(() => ResultadosZona, (resultadosZona) => resultadosZona.partidos, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
  @JoinColumn([{ name: 'id_resultados_zona', referencedColumnName: 'idResultado' }])
  resultadosZona: ResultadosZona;

  @ManyToOne(() => Resultados, (resultados) => resultados.partidos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_resultado', referencedColumnName: 'idResultado' }])
  resultado: Resultados;

  @ManyToOne(() => Canchas, (canchas) => canchas.partidos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_cancha', referencedColumnName: 'idCancha' }])
  cancha: Canchas;

  @ManyToOne(() => Arbitros, (arbitros) => arbitros.partidos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_arbitro', referencedColumnName: 'idArbitro' }])
  arbitro: Arbitros;

  @ManyToOne(() => Veedores, (veedores) => veedores.partidos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_veedor', referencedColumnName: 'idVeedor' }])
  veedor: Veedores;

  @OneToMany(() => Sanciones, (sanciones) => sanciones.partido)
  sanciones: Sanciones[];
}
