import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Domicilios } from './Domicilios';
import { Provincias } from './Provincias';

@Index('fk_localidad_provincia_idx', ['idProvincia'], {})
@Entity('localidades', { schema: 'torneo' })
export class Localidades {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_localidad' })
  idLocalidad: number;

  @Column('varchar', { name: 'n_localidad', length: 45 })
  nLocalidad: string;

  @Column('int', { name: 'id_provincia', nullable: true })
  idProvincia: number | null;

  @Column('datetime', {
    name: 'fecha_alta',
    default: () => 'CURRENT_TIMESTAMP'
  })
  fechaAlta: Date;

  @Column('datetime', { name: 'fecha_modificacion', nullable: true })
  fechaModificacion: Date | null;

  @Column('datetime', { name: 'fecha_baja', nullable: true })
  fechaBaja: Date | null;

  @OneToMany(() => Domicilios, (domicilios) => domicilios.localidad)
  domicilios: Domicilios[];

  @ManyToOne(() => Provincias, (provincias) => provincias.localidades, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_provincia', referencedColumnName: 'idProvincia' }])
  provincia: Provincias;
}
