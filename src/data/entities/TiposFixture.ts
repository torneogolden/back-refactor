import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Fixture } from './Fixture';
import { FixtureZona } from './FixtureZona';

@Entity('tipos_fixture', { schema: 'torneo' })
export class TiposFixture {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_tipo' })
  idTipo: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => Fixture, (fixture) => fixture.tipo)
  fixtures: Fixture[];

  @OneToMany(() => FixtureZona, (fixtureZona) => fixtureZona.tipo)
  fixtureZonas: FixtureZona[];
}
