import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, TableInheritance } from 'typeorm';
import { Arbitros } from './Arbitros';
import { Jugadores } from './Jugadores';
import { Nacionalidades } from './Nacionalidades';
import { EstadosCivil } from './EstadosCivil';
import { TiposDocumento } from './TiposDocumento';
import { Contactos } from './Contactos';
import { Files } from './Files';
import { Domicilios } from './Domicilios';
import { Veedores } from './Veedores';

@Index('nro_documento_UNIQUE', ['nroDocumento'], { unique: true })
@Index('IDX_076bbaBbbbcc', ['nroDocumento'], { unique: true })
@Index('fk_persona_tipo_doc_idx', ['idTipoDocumento'], {})
@Index('fk_persona_domicilio_idx', ['idDomicilio'], {})
@Index('fk_persona_contacto_idx', ['idContacto'], {})
@Index('fk_persona_estado_civil_idx', ['idEstadoCivil'], {})
@Index('fk_persona_nacionalidad_idx', ['idNacionalidad'], {})
@Index('fk_persona_foto_idx', ['idFoto'], {})
@Entity('personas', { schema: 'torneo' })
export class Personas {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_persona' })
  idPersona: number;

  @Column('varchar', { name: 'nombre', length: 45 })
  nombre: string;

  @Column('varchar', { name: 'apellido', length: 45 })
  apellido: string;

  @Column('bigint', { name: 'nro_documento', unique: true })
  nroDocumento: string;

  @Column('int', { name: 'id_tipo_documento' })
  idTipoDocumento: number;

  @Column('date', { name: 'fecha_nacimiento' })
  fechaNacimiento: string;

  @Column('int', { name: 'id_domicilio', nullable: true })
  idDomicilio: number | null;

  @Column('int', { name: 'id_contacto', nullable: true })
  idContacto: number | null;

  @Column('int', { name: 'id_estado_civil', nullable: true })
  idEstadoCivil: number | null;

  @Column('int', { name: 'id_nacionalidad', nullable: true })
  idNacionalidad: number | null;

  @Column('datetime', {
    name: 'fecha_alta',
    default: () => 'CURRENT_TIMESTAMP'
  })
  fechaAlta: Date;

  @Column('datetime', { name: 'fecha_modificacion', nullable: true })
  fechaModificacion: Date | null;

  @Column('datetime', { name: 'fecha_baja', nullable: true })
  fechaBaja: Date | null;

  @Column('int', { name: 'id_foto', nullable: true })
  idFoto: number | null;

  @Column('varchar', { name: 'ocupacion', nullable: true, length: 45 })
  ocupacion: string | null;

  @OneToMany(() => Arbitros, (arbitros) => arbitros.persona)
  arbitros: Arbitros[];

  @OneToMany(() => Jugadores, (jugadores) => jugadores.persona)
  jugadores: Jugadores[];

  @ManyToOne(() => Nacionalidades, (nacionalidades) => nacionalidades.personas, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
  @JoinColumn([{ name: 'id_nacionalidad', referencedColumnName: 'idNacionalidad' }])
  nacionalidad: Nacionalidades;

  @ManyToOne(() => EstadosCivil, (estadosCivil) => estadosCivil.personas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_estado_civil', referencedColumnName: 'idEstadoCivil' }])
  estadoCivil: EstadosCivil;

  @ManyToOne(() => TiposDocumento, (tiposDocumento) => tiposDocumento.personas, { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })
  @JoinColumn([{ name: 'id_tipo_documento', referencedColumnName: 'idTipoDocumento' }])
  tipoDocumento: TiposDocumento;

  @ManyToOne(() => Contactos, (contactos) => contactos.personas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_contacto', referencedColumnName: 'idContacto' }])
  contacto: Contactos;

  @ManyToOne(() => Files, (files) => files.personas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_foto', referencedColumnName: 'id' }])
  foto: Files;

  @ManyToOne(() => Domicilios, (domicilios) => domicilios.personas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_domicilio', referencedColumnName: 'idDomicilio' }])
  domicilio: Domicilios;

  @OneToMany(() => Veedores, (veedores) => veedores.persona)
  veedores: Veedores[];
}
