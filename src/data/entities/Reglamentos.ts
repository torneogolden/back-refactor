import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('fk_reglamento_id_torneo_idx', ['idTorneo'], {})
@Entity('reglamentos', { schema: 'torneo' })
export class Reglamentos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_reglamento' })
  idReglamento: number;

  @Column('mediumtext', { name: 'descripcion', nullable: true })
  descripcion: string | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;
}
