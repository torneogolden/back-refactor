import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Files } from './Files';

@Entity('tipos_archivo', { schema: 'torneo' })
export class TiposArchivo {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'nombre', nullable: true, length: 45 })
  nombre: string | null;

  @OneToMany(() => Files, (files) => files.tipoArchivo)
  files: Files[];
}
