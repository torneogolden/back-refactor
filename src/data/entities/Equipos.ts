import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Clubes } from './Clubes';
import { Torneos } from './Torneos';
import { Categorias } from './Categorias';
import { EquiposZona } from './EquiposZona';
import { Files } from './Files';
import { Fotos } from './Fotos';
import { Goleadores } from './Goleadores';
import { Goles } from './Goles';
import { Jugadores } from './Jugadores';
import { Partidos } from './Partidos';
import { Posiciones } from './Posiciones';
import { RepresentanteEquipo } from './RepresentanteEquipo';
import { Resultados } from './Resultados';
import { ResultadosZona } from './ResultadosZona';
import { SancionesEquipo } from './SancionesEquipo';

@Index('fk_equipo_club_idx', ['idClub'], {})
@Index('fk_equipo_torneo_idx', ['idTorneo'], {})
@Index('fk_equipo_categoria_idx', ['idCategoriaEquipo'], {})
@Entity('equipos', { schema: 'torneo' })
export class Equipos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_equipo' })
  idEquipo: number;

  @Column('varchar', { name: 'nombre', nullable: true, length: 45 })
  nombre: string | null;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @Column('date', { name: 'fecha_alta', nullable: true })
  fechaAlta: string | null;

  @Column('int', { name: 'id_club', nullable: true })
  idClub: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_categoria_equipo', nullable: true })
  idCategoriaEquipo: number | null;

  @ManyToOne(() => Clubes, (clubes) => clubes.equipos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_club', referencedColumnName: 'idClub' }])
  club: Clubes;

  @ManyToOne(() => Torneos, (torneos) => torneos.equipos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Categorias, (categorias) => categorias.equipos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_categoria_equipo', referencedColumnName: 'idCategoria' }])
  categoriaEquipo: Categorias;

  @OneToMany(() => EquiposZona, (equiposZona) => equiposZona.equipo)
  equiposZonas: EquiposZona[];

  @OneToMany(() => Files, (files) => files.equipo)
  files: Files[];

  @OneToMany(() => Fotos, (fotos) => fotos.equipo)
  fotos: Fotos[];

  @OneToMany(() => Goleadores, (goleadores) => goleadores.equipo)
  goleadores: Goleadores[];

  @OneToMany(() => Goles, (goles) => goles.equipo)
  goles: Goles[];

  @OneToMany(() => Jugadores, (jugadores) => jugadores.equipo)
  jugadores: Jugadores[];

  @OneToMany(() => Partidos, (partidos) => partidos.localB)
  partidos: Partidos[];

  @OneToMany(() => Partidos, (partidos) => partidos.visitanteB)
  partidosB: Partidos[];

  @OneToMany(() => Posiciones, (posiciones) => posiciones.equipo)
  posiciones: Posiciones[];

  @OneToMany(() => RepresentanteEquipo, (representanteEquipo) => representanteEquipo.equipo)
  representanteEquipos: RepresentanteEquipo[];

  @OneToMany(() => Resultados, (resultados) => resultados.perdedor)
  resultados: Resultados[];

  @OneToMany(() => Resultados, (resultados) => resultados.ganador)
  resultadosB: Resultados[];

  @OneToMany(() => ResultadosZona, (resultadosZona) => resultadosZona.ganador)
  resultadosZonas: ResultadosZona[];

  @OneToMany(() => ResultadosZona, (resultadosZona) => resultadosZona.perdedor)
  resultadosZonasB: ResultadosZona[];
}
