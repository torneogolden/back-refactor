import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Torneos } from './Torneos';
import { Jugadores } from './Jugadores';
import { Partidos } from './Partidos';
import { Equipos } from './Equipos';

@Index('fk_jugador_gol_idx', ['idJugador'], {})
@Index('fk_partido_gol_idx', ['idPartido'], {})
@Index('fk_equipo_gol_idx', ['idEquipo'], {})
@Index('fk_torneo_gol_idx', ['idTorneo'], {})
@Entity('goles', { schema: 'torneo' })
export class Goles {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_gol' })
  idGol: number;

  @Column('time', { name: 'minuto', nullable: true })
  minuto: string | null;

  @Column('int', { name: 'id_jugador', nullable: true })
  idJugador: number | null;

  @Column('int', { name: 'id_partido', nullable: true })
  idPartido: number | null;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @ManyToOne(() => Torneos, (torneos) => torneos.goles, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Jugadores, (jugadores) => jugadores.goles, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_jugador', referencedColumnName: 'idJugador' }])
  jugador: Jugadores;

  @ManyToOne(() => Partidos, (partidos) => partidos.goles, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_partido', referencedColumnName: 'idPartido' }])
  partido: Partidos;

  @ManyToOne(() => Equipos, (equipos) => equipos.goles, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;
}
