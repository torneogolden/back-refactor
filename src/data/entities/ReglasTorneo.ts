import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Torneos } from './Torneos';

@Index('fk_reglas_torneo_idx', ['idTorneo'], {})
@Entity('reglas_torneo', { schema: 'torneo' })
export class ReglasTorneo {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_regla' })
  idRegla: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 200 })
  descripcion: string | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @ManyToOne(() => Torneos, (torneos) => torneos.reglasTorneos, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @OneToMany(() => Torneos, (torneos) => torneos.regla)
  torneos: Torneos[];
}
