import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { EquiposZona } from './EquiposZona';
import { FixtureZona } from './FixtureZona';
import { PosicionesZona } from './PosicionesZona';
import { Sanciones } from './Sanciones';
import { Fases } from './Fases';
import { Torneos } from './Torneos';

@Index('fk_zonas_torneo_idx', ['idTorneo'], {})
@Index('fk_zona_fase_idx', ['idFase'], {})
@Entity('zonas', { schema: 'torneo' })
export class Zonas {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_zona' })
  idZona: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_fase' })
  idFase: number;

  @OneToMany(() => EquiposZona, (equiposZona) => equiposZona.zona)
  equiposZonas: EquiposZona[];

  @OneToMany(() => FixtureZona, (fixtureZona) => fixtureZona.zona)
  fixtureZonas: FixtureZona[];

  @OneToMany(() => PosicionesZona, (posicionesZona) => posicionesZona.zona)
  posicionesZonas: PosicionesZona[];

  @OneToMany(() => Sanciones, (sanciones) => sanciones.zona)
  sanciones: Sanciones[];

  @ManyToOne(() => Fases, (fases) => fases.zonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_fase', referencedColumnName: 'idFase' }])
  fase: Fases;

  @ManyToOne(() => Torneos, (torneos) => torneos.zonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;
}
