import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Torneos } from './Torneos';
import { Equipos } from './Equipos';

@Index('fk_posicion_equipo_idx', ['idEquipo'], {})
@Index('fk_posicion_torneo_idx', ['idTorneo'], {})
@Entity('posiciones', { schema: 'torneo' })
export class Posiciones {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_posicion' })
  idPosicion: number;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @Column('int', { name: 'puntos', nullable: true })
  puntos: number | null;

  @Column('int', { name: 'goles_favor', nullable: true })
  golesFavor: number | null;

  @Column('int', { name: 'goles_contra', nullable: true })
  golesContra: number | null;

  @Column('int', { name: 'dif_gol', nullable: true })
  difGol: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'partidos_jugados', nullable: true })
  partidosJugados: number | null;

  @Column('int', { name: 'partidos_ganados', nullable: true })
  partidosGanados: number | null;

  @Column('int', { name: 'partidos_empatados', nullable: true })
  partidosEmpatados: number | null;

  @Column('int', { name: 'partidos_perdidos', nullable: true })
  partidosPerdidos: number | null;

  @Column('int', { name: 'fair_play', nullable: true })
  fairPlay: number | null;

  @ManyToOne(() => Torneos, (torneos) => torneos.posiciones, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Equipos, (equipos) => equipos.posiciones, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;
}
