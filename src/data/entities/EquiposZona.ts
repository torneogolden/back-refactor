import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Zonas } from './Zonas';
import { Equipos } from './Equipos';
import { Torneos } from './Torneos';
import { PosicionesZona } from './PosicionesZona';

@Index('fk_equipo_zona_idx', ['idEquipo'], {})
@Index('fk_zona_zonas_idx', ['idZona'], {})
@Index('fk_torneo_zona_idx', ['idTorneo'], {})
@Entity('equipos_zona', { schema: 'torneo' })
export class EquiposZona {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_equipos_zona' })
  idEquiposZona: number;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @Column('int', { name: 'id_zona', nullable: true })
  idZona: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @ManyToOne(() => Zonas, (zonas) => zonas.equiposZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_zona', referencedColumnName: 'idZona' }])
  zona: Zonas;

  @ManyToOne(() => Equipos, (equipos) => equipos.equiposZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;

  @ManyToOne(() => Torneos, (torneos) => torneos.equiposZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @OneToMany(() => PosicionesZona, (posicionesZona) => posicionesZona.equipo)
  posicionesZonas: PosicionesZona[];
}
