import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Zonas } from './Zonas';
import { Torneos } from './Torneos';
import { EquiposZona } from './EquiposZona';

@Index('fk_equipo_posicion_zona_idx', ['idEquipo'], {})
@Index('fk_torneo_posicion_zona_idx', ['idTorneo'], {})
@Index('fk_posicion_zona_idx', ['idZona'], {})
@Entity('posiciones_zona', { schema: 'torneo' })
export class PosicionesZona {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_posicion' })
  idPosicion: number;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @Column('int', { name: 'puntos', nullable: true })
  puntos: number | null;

  @Column('int', { name: 'goles_favor', nullable: true })
  golesFavor: number | null;

  @Column('int', { name: 'goles_contra', nullable: true })
  golesContra: number | null;

  @Column('int', { name: 'dif_gol', nullable: true })
  difGol: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_zona', nullable: true })
  idZona: number | null;

  @Column('int', { name: 'partidos_jugados', nullable: true })
  partidosJugados: number | null;

  @Column('int', { name: 'partidos_ganados', nullable: true })
  partidosGanados: number | null;

  @Column('int', { name: 'partidos_empatados', nullable: true })
  partidosEmpatados: number | null;

  @Column('int', { name: 'partidos_perdidos', nullable: true })
  partidosPerdidos: number | null;

  @Column('int', { name: 'fair_play', nullable: true })
  fairPlay: number | null;

  @ManyToOne(() => Zonas, (zonas) => zonas.posicionesZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_zona', referencedColumnName: 'idZona' }])
  zona: Zonas;

  @ManyToOne(() => Torneos, (torneos) => torneos.posicionesZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => EquiposZona, (equiposZona) => equiposZona.posicionesZonas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: EquiposZona;
}
