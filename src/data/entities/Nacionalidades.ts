import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Personas } from './Personas';

@Index('n_nacionalidad_UNIQUE', ['nNacionalidad'], { unique: true })
@Index('IDX_03469', ['nNacionalidad'], { unique: true })
@Entity('nacionalidades', { schema: 'torneo' })
export class Nacionalidades {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_nacionalidad' })
  idNacionalidad: number;

  @Column('varchar', { name: 'n_nacionalidad', unique: true, length: 45 })
  nNacionalidad: string;

  @Column('datetime', {
    name: 'fecha_alta',
    default: () => 'CURRENT_TIMESTAMP'
  })
  fechaAlta: Date;

  @Column('datetime', { name: 'fecha_modificacion', nullable: true })
  fechaModificacion: Date | null;

  @Column('datetime', { name: 'fecha_baja', nullable: true })
  fechaBaja: Date | null;

  @OneToMany(() => Personas, (personas) => personas.nacionalidad)
  personas: Personas[];
}
