import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Localidades } from './Localidades';

@Entity('provincias', { schema: 'torneo' })
export class Provincias {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_provincia' })
  idProvincia: number;

  @Column('varchar', { name: 'n_provincia', length: 45 })
  nProvincia: string;

  @Column('datetime', {
    name: 'fecha_alta',
    default: () => 'CURRENT_TIMESTAMP'
  })
  fechaAlta: Date;

  @Column('datetime', { name: 'fecha_modificacion', nullable: true })
  fechaModificacion: Date | null;

  @Column('datetime', { name: 'fecha_baja', nullable: true })
  fechaBaja: Date | null;

  @OneToMany(() => Localidades, (localidades) => localidades.provincia)
  localidades: Localidades[];
}
