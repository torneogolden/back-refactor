import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Canchas } from './Canchas';
import { Localidades } from './Localidades';
import { Personas } from './Personas';

@Index('fk_domicilio_localidad_idx', ['idLocalidad'], {})
@Entity('domicilios', { schema: 'torneo' })
export class Domicilios {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_domicilio' })
  idDomicilio: number;

  @Column('varchar', { name: 'calle', length: 45 })
  calle: string;

  @Column('int', { name: 'numeracion' })
  numeracion: number;

  @Column('int', { name: 'piso', nullable: true })
  piso: number | null;

  @Column('varchar', { name: 'dpto', nullable: true, length: 4 })
  dpto: string | null;

  @Column('int', { name: 'torre', nullable: true })
  torre: number | null;

  @Column('int', { name: 'id_localidad', nullable: true })
  idLocalidad: number | null;

  @Column('int', { name: 'codigo_postal', nullable: true })
  codigoPostal: number | null;

  @Column('datetime', {
    name: 'fecha_alta',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP'
  })
  fechaAlta: Date | null;

  @Column('datetime', { name: 'fecha_modificacion', nullable: true })
  fechaModificacion: Date | null;

  @Column('datetime', { name: 'fecha_baja', nullable: true })
  fechaBaja: Date | null;

  @Column('varchar', { name: 'observaciones', nullable: true, length: 500 })
  observaciones: string | null;

  @Column('varchar', { name: 'barrio', nullable: true, length: 45 })
  barrio: string | null;

  @OneToMany(() => Canchas, (canchas) => canchas.domicilio)
  canchas: Canchas[];

  @ManyToOne(() => Localidades, (localidades) => localidades.domicilios, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_localidad', referencedColumnName: 'idLocalidad' }])
  localidad: Localidades;

  @OneToMany(() => Personas, (personas) => personas.domicilio)
  personas: Personas[];
}
