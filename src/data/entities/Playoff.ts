import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Equipos } from '..';
import { EtapaPlayoff } from './EtapaPlayoff';
import { Llaves } from './Llaves';
import { Partidos } from './Partidos';
import { Torneos } from './Torneos';

@Index('fk_playoff_etapa_idx', ['idEtapa'], {})
@Index('fk_playoff_torneo_idx', ['idTorneo'], {})
@Index('fk_equipo_ganador_idx', ['idGanador'], {})
@Index('fk_equipo_local_idx', ['idLocal'], {})
@Index('fk_equipo_visitante_idx', ['idVisitante'], {})
@Index('fk_llave_playoff_idx', ['idLlave'], {})
@Index('fk_partido_playoff_idx', ['idPartido'], {})
@Entity('playoff', { schema: 'torneo' })
export class Playoff {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_playoff' })
  idPlayoff: number;

  @Column('int', { name: 'id_llave', nullable: true })
  idLlave: number | null;

  @Column('int', { name: 'id_local', nullable: true })
  idLocal: number | null;

  @Column('int', { name: 'id_visitante', nullable: true })
  idVisitante: number | null;

  @Column('int', { name: 'id_ganador', nullable: true })
  idGanador: number | null;

  @Column('int', { name: 'id_etapa', nullable: true })
  idEtapa: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_partido', nullable: true })
  idPartido: number | null;

  @ManyToOne(() => Equipos)
  @JoinColumn([{ name: 'id_ganador', referencedColumnName: 'idEquipo' }])
  ganador: Equipos;

  @ManyToOne(() => Equipos)
  @JoinColumn([{ name: 'id_local', referencedColumnName: 'idEquipo' }])
  local: Equipos;

  @ManyToOne(() => Equipos)
  @JoinColumn([{ name: 'id_visitante', referencedColumnName: 'idEquipo' }])
  visitante: Equipos;

  @ManyToOne(() => Torneos)
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Partidos)
  @JoinColumn([{ name: 'id_partido', referencedColumnName: 'idPartido' }])
  partido: Partidos;

  @ManyToOne(() => Llaves)
  @JoinColumn([{ name: 'id_llave', referencedColumnName: 'idLlave' }])
  llave: Llaves;

  @ManyToOne(() => EtapaPlayoff)
  @JoinColumn([{ name: 'id_etapa', referencedColumnName: 'idEtapa' }])
  etapa: EtapaPlayoff;
}
