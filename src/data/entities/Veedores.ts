import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Partidos } from './Partidos';
import { Personas } from './Personas';

@Index('fk_veedor_persona_idx', ['idPersona'], {})
@Entity('veedores', { schema: 'torneo' })
export class Veedores {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_veedor' })
  idVeedor: number;

  @Column('date', { name: 'fecha_alta', nullable: true })
  fechaAlta: string | null;

  @Column('int', { name: 'id_persona', nullable: true })
  idPersona: number | null;

  @OneToMany(() => Partidos, (partidos) => partidos.veedor)
  partidos: Partidos[];

  @ManyToOne(() => Personas, (personas) => personas.veedores, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_persona', referencedColumnName: 'idPersona' }])
  persona: Personas;
}
