import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Clubes } from './Clubes';
import { Torneos } from './Torneos';

@Index('fk_sponsor_club_idx', ['idClub'], {})
@Index('fk_sponsor_torneo_idx', ['idTorneo'], {})
@Entity('sponsors', { schema: 'torneo' })
export class Sponsors {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_sponsor' })
  idSponsor: number;

  @Column('varchar', { name: 'nombre', nullable: true, length: 45 })
  nombre: string | null;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 200 })
  descripcion: string | null;

  @Column('blob', { name: 'logo', nullable: true })
  logo: Buffer | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'id_club', nullable: true })
  idClub: number | null;

  @ManyToOne(() => Clubes, (clubes) => clubes.sponsors, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_club', referencedColumnName: 'idClub' }])
  club: Clubes;

  @ManyToOne(() => Torneos, (torneos) => torneos.sponsors, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;
}
