import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Domicilios } from './Domicilios';
import { Clubes } from './Clubes';
import { Partidos } from './Partidos';
import { TiposCancha } from './TiposCancha';

@Index('fk_cancha_domicilio_idx', ['idDomicilio'], {})
@Index('fk_club_cancha_idx', ['idClub'], {})
@Entity('canchas', { schema: 'torneo' })
export class Canchas {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_cancha' })
  idCancha: number;

  @Column('varchar', { name: 'nombre', nullable: true, length: 45 })
  nombre: string | null;

  @Column('float', { name: 'capacidad', nullable: true, precision: 12 })
  capacidad: number | null;

  @Column('int', { name: 'id_domicilio', nullable: true })
  idDomicilio: number | null;

  @Column('int', { name: 'id_club', nullable: true })
  idClub: number | null;

  @ManyToOne(() => Domicilios, (domicilios) => domicilios.canchas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_domicilio', referencedColumnName: 'idDomicilio' }])
  domicilio: Domicilios;

  @ManyToOne(() => Clubes, (clubes) => clubes.canchas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_club', referencedColumnName: 'idClub' }])
  club: Clubes;

  @OneToMany(() => Partidos, (partidos) => partidos.cancha)
  partidos: Partidos[];

  @OneToMany(() => TiposCancha, (tiposCancha) => tiposCancha.cancha)
  tiposCanchas: TiposCancha[];
}
