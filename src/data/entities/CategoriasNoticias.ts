import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Noticias } from './Noticias';

@Entity('categorias_noticias', { schema: 'torneo' })
export class CategoriasNoticias {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_categoria_noticia' })
  idCategoriaNoticia: number;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 45 })
  descripcion: string | null;

  @OneToMany(() => Noticias, (noticias) => noticias.categoriaNoticia)
  noticias: Noticias[];
}
