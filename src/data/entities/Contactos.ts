import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Personas } from './Personas';

@Entity('contactos', { schema: 'torneo' })
export class Contactos {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_contacto' })
  idContacto: number;

  @Column('varchar', { name: 'telefono_fijo', nullable: true, length: 50 })
  telefonoFijo: string | null;

  @Column('varchar', { name: 'telefono_movil', nullable: true, length: 50 })
  telefonoMovil: string | null;

  @Column('varchar', { name: 'email', nullable: true, length: 45 })
  email: string | null;

  @Column('datetime', {
    name: 'fecha_alta',
    default: () => 'CURRENT_TIMESTAMP'
  })
  fechaAlta: Date;

  @Column('datetime', { name: 'fecha_modificacion', nullable: true })
  fechaModificacion: Date | null;

  @Column('datetime', { name: 'fecha_baja', nullable: true })
  fechaBaja: Date | null;

  @Column('bigint', { name: 'telefono_referencia', nullable: true })
  telefonoReferencia: string | null;

  @OneToMany(() => Personas, (personas) => personas.contacto)
  personas: Personas[];
}
