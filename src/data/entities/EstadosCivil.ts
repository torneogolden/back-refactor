import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Personas } from './Personas';

@Index('n_estado_civil_UNIQUE', ['nEstadoCivil'], { unique: true })
@Index('IDX_ac969', ['nEstadoCivil'], { unique: true })
@Entity('estados_civil', { schema: 'torneo' })
export class EstadosCivil {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_estado_civil' })
  idEstadoCivil: number;

  @Column('varchar', { name: 'n_estado_civil', unique: true, length: 45 })
  nEstadoCivil: string;

  @Column('datetime', {
    name: 'fecha_alta',
    default: () => 'CURRENT_TIMESTAMP'
  })
  fechaAlta: Date;

  @Column('datetime', { name: 'fecha_modificacion', nullable: true })
  fechaModificacion: Date | null;

  @Column('datetime', { name: 'fecha_baja', nullable: true })
  fechaBaja: Date | null;

  @OneToMany(() => Personas, (personas) => personas.estadoCivil)
  personas: Personas[];
}
