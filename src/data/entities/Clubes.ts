import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Canchas } from './Canchas';
import { Equipos } from './Equipos';
import { Fotos } from './Fotos';
import { Noticias } from './Noticias';
import { Sponsors } from './Sponsors';

@Entity('clubes', { schema: 'torneo' })
export class Clubes {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_club' })
  idClub: number;

  @Column('varchar', { name: 'nombre', nullable: true, length: 45 })
  nombre: string | null;

  @Column('varchar', { name: 'descripcion', nullable: true, length: 200 })
  descripcion: string | null;

  @OneToMany(() => Canchas, (canchas) => canchas.club)
  canchas: Canchas[];

  @OneToMany(() => Equipos, (equipos) => equipos.club)
  equipos: Equipos[];

  @OneToMany(() => Fotos, (fotos) => fotos.club)
  fotos: Fotos[];

  @OneToMany(() => Noticias, (noticias) => noticias.club)
  noticias: Noticias[];

  @OneToMany(() => Sponsors, (sponsors) => sponsors.club)
  sponsors: Sponsors[];
}
