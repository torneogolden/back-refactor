import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Equipos } from './Equipos';
import { Torneos } from './Torneos';
import { Jugadores } from './Jugadores';

@Index('fk_jugador_goleador_idx', ['idJugador'], {})
@Index('fk_torneo_goleador_idx', ['idTorneo'], {})
@Index('fk_equipo_goleador_idx', ['idEquipo'], {})
@Entity('goleadores', { schema: 'torneo' })
export class Goleadores {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id_goleador' })
  idGoleador: number;

  @Column('int', { name: 'id_jugador', nullable: true })
  idJugador: number | null;

  @Column('int', { name: 'id_torneo', nullable: true })
  idTorneo: number | null;

  @Column('int', { name: 'cantidad_goles', nullable: true })
  cantidadGoles: number | null;

  @Column('int', { name: 'id_equipo', nullable: true })
  idEquipo: number | null;

  @ManyToOne(() => Equipos, (equipos) => equipos.goleadores, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_equipo', referencedColumnName: 'idEquipo' }])
  equipo: Equipos;

  @ManyToOne(() => Torneos, (torneos) => torneos.goleadores, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_torneo', referencedColumnName: 'idTorneo' }])
  torneo: Torneos;

  @ManyToOne(() => Jugadores, (jugadores) => jugadores.goleadores, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  })
  @JoinColumn([{ name: 'id_jugador', referencedColumnName: 'idJugador' }])
  jugador: Jugadores;
}
