import { Request, Response } from 'express';
import { TYPES } from '../services/types/types';
import container from '../services/inversify.config';
import { logger } from '../logger/CustomLogger';
import { HttpCodes } from '../models';
import { NoticiaService } from '../services/implementations';

const _noticiaService = container.get<NoticiaService>(TYPES.NoticiaService);

export async function mainNews(request: Request, response: Response): Promise<Response> {
  return _noticiaService
    .mainNews(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function secondaryNews(request: Request, response: Response): Promise<Response> {
  return _noticiaService
    .secondaryNews(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function getById(request: Request, response: Response): Promise<Response> {
  return _noticiaService
    .getById(+request.params.idNews)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function historicalNews(request: Request, response: Response): Promise<Response> {
  return _noticiaService
    .historicalNews(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export const NoticiaController = {
  mainNews,
  secondaryNews,
  getById,
  historicalNews
};
