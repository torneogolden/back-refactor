import { Request, Response } from 'express';
import { TYPES } from '../services/types/types';
import container from '../services/inversify.config';
import { logger } from '../logger/CustomLogger';
import { HttpCodes } from '../models';
import { CanchaService } from '../services/implementations/index';

const _canchaService = container.get<CanchaService>(TYPES.CanchaService);

export async function getAll(request: Request, response: Response): Promise<Response> {
  return _canchaService
    .getAll()
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function register(request: Request, response: Response): Promise<Response> {
  return _canchaService
    .register(request.body)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export const CanchaController = {
  getAll,
  register
};
