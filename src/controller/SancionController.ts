import { Request, Response } from 'express';
import { TYPES } from '../services/types/types';
import container from '../services/inversify.config';
import { logger } from '../logger/CustomLogger';
import { HttpCodes } from '../models';
import { SancionService } from '../services/implementations/index';

const _sanctionService = container.get<SancionService>(TYPES.SancionService);

export async function getTeamSanctions(request: Request, response: Response): Promise<Response> {
  return _sanctionService
    .getTeamSanctions(request.body, +request.params.idPashe)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export const SancionController = {
  getTeamSanctions
};
