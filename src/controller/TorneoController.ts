import { Request, Response } from 'express';
import { TYPES } from '../services/types/types';
import container from '../services/inversify.config';
import { logger } from '../logger/CustomLogger';
import { HttpCodes } from '../models';
import { TorneoService } from '../services/implementations';

const _tournamentService = container.get<TorneoService>(TYPES.TorneoService);

export async function getCurrent(request: Request, response: Response): Promise<Response> {
  return _tournamentService
    .getCurrent()
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function getAllTeamsByTournament(request: Request, response: Response): Promise<Response> {
  return _tournamentService
    .getAllTeamsByTournament(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function getByName(request: Request, response: Response): Promise<Response> {
  return _tournamentService
    .getByName(request.params.name)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function getScorersByTournament(request: Request, response: Response): Promise<Response> {
  return _tournamentService
    .getScorersByTournament(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export const TorneoController = {
  getCurrent,
  getAllTeamsByTournament,
  getByName,
  getScorersByTournament
};
