import { Request, Response } from 'express';
import { TYPES } from '../services/types/types';
import container from '../services/inversify.config';
import { logger } from '../logger/CustomLogger';
import { HttpCodes } from '../models';
import { ReglamentoService } from '../services/implementations/index';

const _reglamentoService = container.get<ReglamentoService>(TYPES.ReglamentoService);

export async function getRegulationById(request: Request, response: Response): Promise<Response> {
  return _reglamentoService
    .getById(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export const ReglamentoController = {
  getRegulationById
};
