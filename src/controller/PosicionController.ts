import { Request, Response } from 'express';
import { TYPES } from '../services/types/types';
import container from '../services/inversify.config';
import { logger } from '../logger/CustomLogger';
import { HttpCodes } from '../models';
import { PosicionService } from '../services/implementations';

const _positionsService = container.get<PosicionService>(TYPES.PosicionService);

export async function getPositions(request: Request, response: Response): Promise<Response> {
  return _positionsService
    .getPositions(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}
export async function getPositionsByZone(request: Request, response: Response): Promise<Response> {
  return _positionsService
    .getPositionsByZone(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export const PosicionController = {
  getPositions,
  getPositionsByZone
};
