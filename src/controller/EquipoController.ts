import { Request, Response } from 'express';
import { TYPES } from '../services/types/types';
import container from '../services/inversify.config';
import { logger } from '../logger/CustomLogger';
import { HttpCodes } from '../models';
import { EquipoService } from '../services/implementations';

const _teamService = container.get<EquipoService>(TYPES.EquipoService);

export async function getTeamById(request: Request, response: Response): Promise<Response> {
  return _teamService
    .getTeamById(+request.params.idTeam)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function getPlayersByTeam(request: Request, response: Response): Promise<Response> {
  return _teamService
    .getPlayersByTeam(+request.params.idTeam, +request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export const EquipoController = {
  getTeamById,
  getPlayersByTeam
};
