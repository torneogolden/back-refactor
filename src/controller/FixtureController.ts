import { Request, Response } from 'express';
import { TYPES } from '../services/types/types';
import container from '../services/inversify.config';
import { logger } from '../logger/CustomLogger';
import { HttpCodes } from '../models';
import { FixtureService } from '../services/implementations/index';

const _fixtureService = container.get<FixtureService>(TYPES.FixtureService);

export async function getFixtureById(request: Request, response: Response): Promise<Response> {
  return _fixtureService
    .getById(+request.params.idTournament, request.body)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function getMatchesById(request: Request, response: Response): Promise<Response> {
  return _fixtureService
    .getMatchesById(+request.params.idTournament, request.body)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function getSchedulesPlayedById(request: Request, response: Response): Promise<Response> {
  return _fixtureService
    .getSchedulesPlayedById(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export async function getMatchesPlayedById(request: Request, response: Response): Promise<Response> {
  return _fixtureService
    .getMatchesPlayedById(+request.params.idTournament, request.body)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}
export async function getPlayoffByTournament(request: Request, response: Response): Promise<Response> {
  return _fixtureService
    .getPlayoffByTournament(+request.params.idTournament)
    .then((x: unknown) => {
      return response.status(HttpCodes.OK).json(x);
    })
    .catch((error: { message: any }) => {
      logger.error(error.message);
      return response.status(HttpCodes.CONFLICT).json(error.message);
    });
}

export const FixtureController = {
  getFixtureById,
  getMatchesById,
  getSchedulesPlayedById,
  getMatchesPlayedById,
  getPlayoffByTournament
};
